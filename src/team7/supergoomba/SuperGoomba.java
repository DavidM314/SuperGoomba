package team7.supergoomba;

import team7.supergoomba.objectinteractionsystems.characters.EnumCharacter;
import team7.supergoomba.userinterfaces.menus.MainMenu;
import team7.supergoomba.userinterfaces.menus.MultiPlayerMenu;
import team7.supergoomba.userinterfaces.menus.SinglePlayerMenu;

public class SuperGoomba {

	private Renderer2D renderer;
	
	public static void main(String[] args) {
		SuperGoomba supergoomba = new SuperGoomba();
		supergoomba.start();
	}
	
	public SuperGoomba() {
		this.renderer = new Renderer2D();
	}
	
	public void start() {
		boolean loop = true;
		while(loop) {
			int gameModeChoice = new MainMenu(renderer).displayMenu();
			Game g;
			if(gameModeChoice==1) {
				SinglePlayerMenu spMenu = new SinglePlayerMenu(renderer);
				int value = spMenu.displayMenu();
				EnumCharacter characterChoice = EnumCharacter.values()[value%100];
				EnumCharacter characterChoice2 = EnumCharacter.values()[value/100];
				g = new Game(characterChoice,characterChoice2,renderer,true);
			} else {
				EnumCharacter characterChoice1 = EnumCharacter.values()[new MultiPlayerMenu(renderer,1).displayMenu()];
				EnumCharacter characterChoice2 = EnumCharacter.values()[new MultiPlayerMenu(renderer,2).displayMenu()];
				g = new Game(characterChoice1, characterChoice2,renderer,false);
			}
			g.start();
			while(g.getChoice()==-1) {
				try {
					Thread.sleep(20);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			loop = g.getChoice()==1;
			renderer.clearPaintQueue();
		}
		stop();
	}
	
	public void stop() {
		System.exit(0);
	}

}
