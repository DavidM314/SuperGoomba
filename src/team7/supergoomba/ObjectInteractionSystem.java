package team7.supergoomba;

import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.Stack;

import team7.supergoomba.objectinteractionsystems.CharacterState;
import team7.supergoomba.objectinteractionsystems.GameCharacter;
import team7.supergoomba.objectinteractionsystems.InGameObject;
import team7.supergoomba.objectinteractionsystems.characters.Boo;
import team7.supergoomba.objectinteractionsystems.characters.ShyGuy;
import team7.supergoomba.objectinteractionsystems.otherobjects.Barrel;
import team7.supergoomba.objectinteractionsystems.otherobjects.Fireball;
import team7.supergoomba.objectinteractionsystems.otherobjects.Projectile;
import team7.supergoomba.renderer2d.PaintableObject;

public class ObjectInteractionSystem implements PaintableObject {
	
	private double groundLevelOffset;
	private ArrayList<InGameObject> gameObjects;
	private double gravitationalAcceleration = 4;
	private Renderer2D renderer;
	private Game game;
	private final int DMG_COOLDOWN_TIME = 600;

	public ObjectInteractionSystem(double groundLevelOffset, Renderer2D renderer, Game g) {
		this.gameObjects = new ArrayList<InGameObject>();
		this.groundLevelOffset = groundLevelOffset;
		this.renderer = renderer;
		this.game = g;
	}

	@Override
	public void paint(Graphics2D g) {
		updateGame();
		for(InGameObject o : gameObjects.toArray(new InGameObject[gameObjects.size()])) {
			o.paint(g);
		}		
	}

	public void updateGame() {
		int screenHeight = renderer.getHeight();
		int screenWidth = renderer.getWidth();
		for(InGameObject o : gameObjects.toArray(new InGameObject[gameObjects.size()])) {
			if(o instanceof Projectile && ((Projectile)o).shouldRemove()) {
				gameObjects.remove(o);
				continue;
			}
			double footPos = o.getY() + o.getHeight();
			double groundLevel = screenHeight - groundLevelOffset;
			for(InGameObject p : gameObjects.toArray(new InGameObject[gameObjects.size()])) {
				if(p==o) continue;
				if(haveCollided(o,p)) {
					if(o instanceof Projectile && p instanceof GameCharacter) {
						((Projectile)o).onCollision((GameCharacter)p);
					} else if(o instanceof GameCharacter && p instanceof GameCharacter) {
						GameCharacter g1 = (GameCharacter)o, g2 = (GameCharacter)p;
						double motionX1 = g1.getMotionX(), motionX2 = g2.getMotionX(),
								motionY1 = g1.getMotionY(), motionY2 = g2.getMotionY();
						
						if((g1 instanceof ShyGuy && ((ShyGuy)g1).isShyStack() && ((g2 instanceof ShyGuy)?!((ShyGuy)g2).isShyStack():true)) || (g1.getStateStack().peek()==CharacterState.ATTACKING || g1.getStateStack().peek()==CharacterState.SPECIAL_ATTACKING || g1.getStateStack().peek()==CharacterState.S_SPECIAL_ATTACKING)) {
							
							if(!(g1 instanceof Boo && g1.getStateStack().peek()==CharacterState.SPECIAL_ATTACKING)) {
								g2.setMotionX(motionX1>=0?50:-50);
								g2.setMotionY(motionY1>=0?30:-30);
								if(g2.getMotionY()>0 && g2.getOnGround()) {
									g2.setOnGround(false);
									g2.setY(g2.getY() - 3);
								}
								if(System.currentTimeMillis()-g1.getLastDamageDealt()>this.DMG_COOLDOWN_TIME) {
									g1.onCollision(g2);
									g1.setLastDamageDealt(System.currentTimeMillis());
									g1.setBotAttackMode(false);
								}
							}
						}
						if(g2.getStateStack().peek()==CharacterState.ATTACKING || g2.getStateStack().peek()==CharacterState.SPECIAL_ATTACKING || g2.getStateStack().peek()==CharacterState.S_SPECIAL_ATTACKING) {
							if(!(g2 instanceof ShyGuy && ((ShyGuy)g2).isShyStack()) && !(g2 instanceof Boo && g2.getStateStack().peek()==CharacterState.SPECIAL_ATTACKING)) {
								g1.setMotionX(motionX2>=0?50:-50);
								g1.setMotionY(motionY2>=0?30:-30);
								if(g1.getMotionY()>0 && g1.getOnGround()) {
									g1.setOnGround(false);
									g1.setY(g1.getY() - 3);
								}
								if(System.currentTimeMillis()-g2.getLastDamageDealt()>this.DMG_COOLDOWN_TIME) {
									g2.onCollision(g1);
									g2.setLastDamageDealt(System.currentTimeMillis());
									g2.setBotAttackMode(false);
								}
							}
						}
					}
				}
			}
			if(footPos<groundLevel && o.getGravityAffected()) {
				if(groundLevel-footPos < -(o.getMotionY()-5)) {
					o.setMotionY(-(groundLevel-footPos));
				} else {
					o.changeMotionY(-gravitationalAcceleration);
				}
				o.setOnGround(false);
			} else if (o.getMotionY()!=0 && !(o instanceof Fireball)) {
				o.setMotionY(0);
				o.setOnGround(true);
				if(o instanceof GameCharacter) {
					Stack<CharacterState> temp = new Stack<CharacterState>() {
						private static final long serialVersionUID = 2038927146936816505L;
						@Override
						public CharacterState peek() {
							if(this.isEmpty()) {
								return null;
							}
							return super.peek();
						}
					};
					GameCharacter gc = (GameCharacter)o;
					while(gc.getStateStack().peek()!=null) {
						temp.push(gc.getStateStack().pop());
					}
					while(temp.peek()!=null) {
						CharacterState value = temp.pop();
						if(value!=CharacterState.JUMPING) {
							gc.getStateStack().push(value);
						}
					}
				}
			}
			if(o.getMotionX()!=0 && o.getGravityAffected()) {
				double frictionConstant = o.getOnGround()?1.9:1.2;
				if(o instanceof Barrel) {
					frictionConstant /= 3.0;
				}
				if(o.getMotionX()>0) {
					if(o.getMotionX()-frictionConstant>=0) {
						o.changeMotionX(-frictionConstant);
					} else {
						o.setMotionX(0);
					}
				} else if (o.getMotionX()<0) {
					if(o.getMotionX()+frictionConstant<=0) {
						o.changeMotionX(frictionConstant);
					} else {
						o.setMotionX(0);
					}
				}
			}
			if(o.getX() + o.getMotionX() + o.getWidth() > screenWidth && o.getGravityAffected()) {
				o.setX(screenWidth-o.getMotionX()-o.getWidth());
			}
			if(o.getX() + o.getMotionX() < 0 && o.getGravityAffected()) {
				o.setX(-o.getMotionX());
			}
			if(footPos>groundLevel && o.getGravityAffected()) {
				o.setY(groundLevel-o.getHeight());
			}
			if(footPos<0 && o.getGravityAffected()) {
				o.setY(0);
				o.setMotionY(-5);
			}
		}
	}

	public boolean isOnGround(InGameObject o) {
		double footPos = o.getY() + o.getHeight();
		double groundLevel = renderer.getHeight() - groundLevelOffset;
		if(footPos>=groundLevel) {
			return true;
		}
		return false;
	}

	public static boolean haveCollided(InGameObject o1, InGameObject o2) {
		double x11 = o1.getX(), x12 = o1.getX() + o1.getWidth(), y11 = o1.getY(), y12 = o1.getY() + o1.getHeight(),
				x21 = o2.getX(), x22 = o2.getX() + o2.getWidth(), y21 = o2.getY(), y22 = o2.getY() + o2.getHeight();
		return ((x21<=x11 && x11<=x22) || (x21<=x12 && x12<=x22) || (x11<=x21 && x21<=x12) || (x11<=x22 && x22<=x12))
				&& ((y21<=y11 && y11<=y22) || (y21<=y12 && y12<=y22) || (y11<=y21 && y21<=y12) || (y11<=y22 && y22<=y12));
	}
	
	public void add(InGameObject o) {
		this.gameObjects.add(o);
	}
		
	public void remove(InGameObject o) {
		if(this.gameObjects.contains(o)) {
			this.gameObjects.remove(o);
		}
	}

	public Renderer2D getRenderer() {
		return this.renderer;
	}
	
	public double getGroundLevelOffset() {
		return this.groundLevelOffset;
	}
	
	public InGameObject[] getInGameObjects() {
		return this.gameObjects.toArray(new InGameObject[this.gameObjects.size()]);
	}
	
	@Deprecated
	public void swap(GameCharacter initial, GameCharacter replacement) {
		gameObjects.remove(initial);
		gameObjects.add(replacement);
		if(initial==game.getPlayer(1)) {
			game.setPlayer(1,replacement);
		} else if (initial==game.getPlayer(2)) {
			game.setPlayer(2,replacement);
		}
		renderer.getFrame().removeKeyListener(initial);
		renderer.getFrame().addKeyListener(replacement);
	}

}
