package team7.supergoomba;

import java.awt.event.KeyListener;

import team7.supergoomba.objectinteractionsystems.GameCharacter;
import team7.supergoomba.objectinteractionsystems.InGameObject;
import team7.supergoomba.objectinteractionsystems.KeyControlEnum;
import team7.supergoomba.objectinteractionsystems.characters.*;
import team7.supergoomba.userinterfaces.Background;
import team7.supergoomba.userinterfaces.huds.*;
import team7.supergoomba.userinterfaces.menus.NewGameMenu;

public class Game {
	private int choice;
	private ObjectInteractionSystem oiSystem;
	private GameCharacter player1;
	private GameCharacter player2;
	private UserInterface ui;
	private long startTime;
	private Renderer2D renderer;
	private Background bg;
	private AudioPlayer audioPlayer;
	private boolean isSinglePlayer;
	private long permanentTime = 1;
	
	public boolean isSinglePlayer() {
		return this.isSinglePlayer;
	}
	
	public void freezeTime() {
		this.permanentTime = System.currentTimeMillis();
	}
	
	public GameCharacter getPlayer(int playerNum) {
		switch(playerNum) {
		case 1:
			return player1;
		case 2:
			return player2;
		default:
			return null;
		}
	}
	
	@Deprecated
	public Game(EnumCharacter characterChoice, Renderer2D renderer) {
		this.renderer = renderer;
		oiSystem = new ObjectInteractionSystem(renderer.getWidth()/10, renderer,this);
		KeyControlEnum controls = KeyControlEnum.WASD_KEYS;
		int x1 = 0, y1 = 0;
		player1 = createCharacter(x1,y1,controls,characterChoice, false,oiSystem);
		player2 = createCharacter(renderer.getWidth()-30,y1,KeyControlEnum.NONE,EnumCharacter.GOOMBA, true,oiSystem);
		player1.setEnemy(player2);
		player2.setEnemy(player1);
		oiSystem.add(player1);
		oiSystem.add(player2);
		bg = new Background(renderer, oiSystem);
		ui = new UserInterface(this, renderer);
		audioPlayer = new AudioPlayer();
		isSinglePlayer = true;
		choice = -1;
	}
	
	public String getTimeStr() {
		int timeSeconds = (int) (((this.permanentTime==1?System.currentTimeMillis():this.permanentTime)-startTime)/1000);
		int minutes = timeSeconds/60;
		int seconds = timeSeconds - (minutes*60);
		return minutes + ":" + (seconds<10?"0":"") + seconds;
	}
	
	public static GameCharacter createCharacter(int x, int y, KeyControlEnum controls, EnumCharacter characterChoice, boolean isBot, ObjectInteractionSystem oiSystem) {
		GameCharacter player1 = null;
		switch(characterChoice) {
		case BOO:
			player1 = new Boo(x,y,controls, oiSystem, isBot);
			break;
		case BOWSER_JR:
			player1 = new BowserJr(x,y,controls, oiSystem, isBot);
			break;
		case BOWSER:
			player1 = new Bowser(x,y,controls, oiSystem, isBot);
			break;
		case DAISY:
			player1 = new Daisy(x,y,controls, oiSystem, isBot);
			break;
		case DIDDY_KONG:
			player1 = new DiddyKong(x,y,controls, oiSystem, isBot);
			break;
		case DONKEY_KONG:
			player1 = new DonkeyKong(x,y,controls, oiSystem, isBot);
			break;
		case GOOMBA:
			player1 = new Goomba(x,y,controls, oiSystem, isBot);
			break;
		case HAMMER_BRO:
			player1 = new HammerBro(x,y,controls, oiSystem, isBot);
			break;
		case KOOPA_TROOPA:
			player1 = new KoopaTroopa(x,y,controls, oiSystem, isBot);
			break;
		case LUIGI:
			player1 = new Luigi(x,y,controls, oiSystem, isBot);
			break;
		case MARIO:
			player1 = new Mario(x,y,controls, oiSystem, isBot);
			break;
		case PEACH:
			player1 = new Peach(x,y,controls, oiSystem, isBot);
			break;
		case SHY_GUY:
			player1 = new ShyGuy(x,y,controls, oiSystem, isBot,false);
			break;
		case SPINY:
			player1 = new Spiny(x,y,controls, oiSystem, isBot);
			break;
		case TOAD:
			player1 = new Toad(x,y,controls, oiSystem, isBot);
			break;
		case YOSHI:
			player1 = new Yoshi(x,y,controls, oiSystem, isBot);
			break;
		}
		return player1;
	}

	public Game(EnumCharacter characterChoice1, EnumCharacter characterChoice2, Renderer2D renderer, boolean singlePlayer) {
		this.renderer = renderer;
		oiSystem = new ObjectInteractionSystem(renderer.getWidth()/10, renderer, this);
		KeyControlEnum controls = KeyControlEnum.WASD_KEYS;
		int x1 = 0, y1 = 0;
		player1 = createCharacter(x1,y1,controls,characterChoice1,false,oiSystem);
		player2 = createCharacter(renderer.getWidth()-30,y1,KeyControlEnum.ARROW_KEYS,characterChoice2,singlePlayer,oiSystem);
		player1.setEnemy(player2);
		player2.setEnemy(player1);
		oiSystem.add(player1);
		oiSystem.add(player2);
		bg = new Background(renderer, oiSystem);
		ui = new UserInterface(this, renderer);
		audioPlayer = new AudioPlayer();
		isSinglePlayer = singlePlayer;
		choice = -1;
	}

	public void setPlayer(int i, GameCharacter gc) {
		if(i==1) {
			player1 = gc;
		} else if (i==2) {
			player2 = gc;
		}
	}
	
	public void start() {
		this.startTime = System.currentTimeMillis();
		renderer.addObjectToPaintQueue(bg);
		renderer.addObjectToPaintQueue(oiSystem);
		renderer.addObjectToPaintQueue(ui);
		renderer.addObjectToPaintQueue(new InstructionsHUD(renderer));
		audioPlayer.startBgMusic();
		renderer.addObjectToPaintQueue(new InstructionsHUD(renderer));
		new Thread() {
			public void run() {
				while((player2==null || player2.getHealth()>0) && (player1==null || player1.getHealth()>0)) {
					try {
						Thread.sleep(20);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				end();
			}
		}.start();
	}
	
	@SuppressWarnings("static-access")
	public void end() {
		audioPlayer.stopBgMusic();
		if(isSinglePlayer) {
			if(player1.getHealth() > 0) {
				audioPlayer.getSoundEffect().END.play();
			} else {
				audioPlayer.getSoundEffect().DIE.play();
			}
		} else {
			audioPlayer.getSoundEffect().END.play();
		}
		freezeTime();
		for(InGameObject o : this.getObjectInteractionSystem().getInGameObjects()) {
			if(o instanceof GameCharacter) {
				((GameCharacter)o).setBot(false);
				this.renderer.removeKeyListener((KeyListener)o);
				((GameCharacter) o).setKeyArray(new boolean[4]);
			}
		}
		this.choice = new NewGameMenu(renderer,this).displayMenu();
	}
	
	public ObjectInteractionSystem getObjectInteractionSystem() {
		return this.oiSystem;
	}

	public int getChoice() {
		return choice;
	}
}