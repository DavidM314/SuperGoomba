package team7.supergoomba;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;

import team7.supergoomba.objectinteractionsystems.GameCharacter;
import team7.supergoomba.renderer2d.PaintableObject;
import team7.supergoomba.userinterfaces.Menu;
import team7.supergoomba.userinterfaces.huds.InstructionsHUD;

public class Renderer2D extends JPanel implements ActionListener {
	
	private static final long serialVersionUID = -1153433080822204529L;
	private JFrame frame;
	private ArrayList<PaintableObject> objectsToPaint;
	private Timer timer;
	
	public Renderer2D() {
		super();
		this.frame = new JFrame("Super Goomba");
		frame.add(this);
		frame.setSize(500, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
		frame.setVisible(true);
		this.setSize(frame.getSize());
		this.objectsToPaint = new ArrayList<PaintableObject>();
		this.timer = new Timer(40, this);
		this.timer.start();
	}
	
	@Override
	public void paint(Graphics graphics) {
		super.paint(graphics);
		Graphics2D g = (Graphics2D)graphics;
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, this.getWidth(), this.getHeight());
		PaintableObject[] paintQueue = objectsToPaint.toArray(new PaintableObject[objectsToPaint.size()]);
		for(PaintableObject o : paintQueue) {
			o.paint(g);
		}
	}
	
	public void addObjectToPaintQueue(PaintableObject o) {
		objectsToPaint.add(o);
		if(o instanceof GameCharacter) {
			this.frame.addKeyListener((GameCharacter)o);
		}
		if(o instanceof InstructionsHUD) {
			this.frame.addKeyListener((InstructionsHUD)o);
		}
		if(o instanceof Menu) {
			this.frame.addMouseListener((Menu)o);
		}
	}

	public void addObjectToPaintQueue(Menu m) {
		objectsToPaint.add(m);
		this.addMouseListener(m);
		this.addMouseMotionListener(m);
	}
	
	public void addObjectToPaintQueue(ObjectInteractionSystem g) {
		objectsToPaint.add(g);
		for(PaintableObject o2 : g.getInGameObjects()) {
			if(o2 instanceof GameCharacter) {
				this.frame.addKeyListener((GameCharacter)o2);
			}
		}
	}
	
	public JFrame getFrame() {
		return this.frame;
	}
	
	public void removeObjectFromPaintQueue(PaintableObject o) {
		objectsToPaint.remove(o);
		if(o instanceof KeyListener) {
			this.removeKeyListener((KeyListener)o);
		}
		if(o instanceof MouseListener) {
			this.removeMouseListener((MouseListener)o);
		}
		if(o instanceof MouseMotionListener) {
			this.removeMouseMotionListener((MouseMotionListener)o);
		}
	}

	public void clearPaintQueue() {
		for(PaintableObject o : objectsToPaint) {
			if(o instanceof KeyListener) {
				this.removeKeyListener((KeyListener)o);
			}
			if(o instanceof MouseListener) {
				this.removeMouseListener((MouseListener)o);
			}
			if(o instanceof MouseMotionListener) {
				this.removeMouseMotionListener((MouseMotionListener)o);
			}
		}
		objectsToPaint = new ArrayList<PaintableObject>();
		this.setCursor(Cursor.getDefaultCursor());
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		this.repaint();
	}
	
}

