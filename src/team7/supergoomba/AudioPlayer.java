package team7.supergoomba;

import java.io.File;
import java.io.IOException;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

public class AudioPlayer {
	private File bgMusic = new File("./resources/sounds/bg_music.wav");
	private AudioInputStream bgMusicAIS;
	private Clip bgClip;
	public enum SoundEffect {
		JUMP("./resources/sounds/jump.wav"),
		ATTACK("./resources/sounds/attack.wav"),
		POWERUP("./resources/sounds/powerup.wav"),
		SHELL("./resources/sounds/shell.wav"),
		LASER("./resources/sounds/laser.wav"),
		EXPLOSION("./resources/sounds/explosion.wav"),
		FIREBALL("./resources/sounds/fireball.wav"),
		WHACK("./resources/sounds/whack.wav"),
		END("./resources/sounds/end.wav"),
		DIE("./resources/sounds/die.wav");
		
		private Clip clip;
		
		SoundEffect(String fileString) {
			try {
			File file = new File(fileString);
			AudioInputStream ais = AudioSystem.getAudioInputStream(file);
			clip = AudioSystem.getClip();
			clip.open(ais);
			} catch (UnsupportedAudioFileException e) {
				
			} catch (IOException e) {
				
			} catch (LineUnavailableException e) {
				
			}
		}
		
		public void play() {
			if (clip.isRunning()) {
				clip.stop();
			}
			clip.setFramePosition(0);
			clip.start();
		}
	}
	private SoundEffect soundEffect;
	
	public AudioPlayer() {
		try {
			bgMusicAIS = AudioSystem.getAudioInputStream(bgMusic);
			bgClip = AudioSystem.getClip();
			bgClip.open(bgMusicAIS);
		} catch (UnsupportedAudioFileException e) {
			System.err.println("Error: The audio file is not supported.");
		} catch (IOException e) {
			System.err.println("Error: Cannot read audio file.");
		} catch (LineUnavailableException e) {
			System.err.println("Error: Line cannot be opened.");
		}
	}
	
	public void startBgMusic() {
		bgClip.setFramePosition(0);
		bgClip.loop(Clip.LOOP_CONTINUOUSLY);
	}
	
	public void stopBgMusic() {
		if (bgClip.isRunning()) {
			bgClip.stop();
		}
	}
	
	public SoundEffect getSoundEffect() {
		return soundEffect;
	}
}