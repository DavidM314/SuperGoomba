package team7.supergoomba.userinterfaces;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import team7.supergoomba.ObjectInteractionSystem;
import team7.supergoomba.Renderer2D;
import team7.supergoomba.renderer2d.PaintableObject;

public class Background implements PaintableObject {
	private Renderer2D renderer;
	private ObjectInteractionSystem oiSystem;
	private int[][] clouds = {
			{-50,20}, {500,100}, {180,120}, {900,130}, {1250,80}, {1100,160}, {300,180}, {100,240}, {630,240}, {800,300}
	};
	private int[][] hills = new int[8][2];
	private Image ground;
	private final File groundFile = new File("./resources/user interfaces/ground.png");
	
	public Background(Renderer2D renderer, ObjectInteractionSystem oiSystem) {
		this.renderer = renderer;
		this.oiSystem = oiSystem;
		int height = renderer.getHeight();
		int[][] h = {
				{1,13*height/20}, {370,1*height/3}, {300,height/2}, {430,6*height/10}, {700,7*height/10}, {1000,7*height/10}, {1120,6*height/10}, {1080,3*height/4}
		};
		hills = h;
		try {
			ground = ImageIO.read(groundFile);
		} catch (IOException e) {}
	}
	
	public void paint(Graphics2D g) {
		int width = renderer.getWidth(), height = renderer.getHeight(), gOffset = (int)oiSystem.getGroundLevelOffset();
		g.setColor(new Color(65, 105, 225));
		g.fillRect(0, 0, width, height);
		for (int[] h : hills) {
			g.setColor(new Color(102, 178, 255));
			g.fillRoundRect(h[0], h[1], 120, height, 120, 120);
			g.setColor(new Color(0, 76, 153));
			g.drawRoundRect(h[0], h[1], 120, height, 120, 120);
		}
		g.setColor(Color.WHITE);
		for (int[] c : clouds) {
			g.fillRoundRect(c[0], c[1], 100, 30, 30, 30);
			if (c[0] > width) {
				c[0] = -100;
			} else {
				c[0] += 1;
			}
		}
		for (int i = 0; i < width; i += gOffset) {
			g.drawImage(ground, i, height-gOffset, gOffset, gOffset, null);
		}
	}
}