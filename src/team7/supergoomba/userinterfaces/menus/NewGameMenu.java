package team7.supergoomba.userinterfaces.menus;

import java.awt.Cursor;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import team7.supergoomba.Game;
import team7.supergoomba.Renderer2D;
import team7.supergoomba.userinterfaces.Menu;

public class NewGameMenu extends Menu {

	private final File quitFile = new File("./resources/menus/quitGame.png");
	private final File mainMenuFile = new File("./resources/menus/mainMenuButton.png");
	private Image result;
	
	public NewGameMenu(Renderer2D renderer, Game g) {
		super(renderer);
		try {
			if(g.isSinglePlayer()) {
				if(g.getPlayer(1).getHealth()<=0) {
					result = ImageIO.read(new File("./resources/menus/you_lose.png"));
				} else {
					result = ImageIO.read(new File("./resources/menus/you_win.png"));
				}
			} else {
				if(g.getPlayer(1).getHealth()<=0) {
					result = ImageIO.read(new File("./resources/menus/player2_wins.png"));
				} else {
					result = ImageIO.read(new File("./resources/menus/player1_wins.png"));
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(2);
		}
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		double width = this.renderer.getWidth();
		double height = this.renderer.getHeight();
		try {
			Image mainMenuButton = ImageIO.read(this.mainMenuFile);
			Image quitGameButton = ImageIO.read(this.quitFile);
			int x1 = (int)(width/2-mainMenuButton.getWidth(null)/2), y1 = (int)(height/2 - mainMenuButton.getHeight(null) - 50);
			int x2 = (int)(width/2-quitGameButton.getWidth(null)/2), y2 = (int)(height/2 + 50);
			int x = arg0.getX();
			int y = arg0.getY();
			if(x1<x && x < x1 + mainMenuButton.getWidth(null) && y1 < y && y < y1 + mainMenuButton.getHeight(null)) {
				this.choice = 1;
			} else if(x2<x && x < x2 + quitGameButton.getWidth(null) && y2 < y && y < y2 + quitGameButton.getHeight(null)) {
				this.choice = 2;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {}

	@Override
	public void mouseExited(MouseEvent arg0) {}

	@Override
	public void mousePressed(MouseEvent arg0) {}

	@Override
	public void mouseReleased(MouseEvent arg0) {}

	@Override
	public void mouseDragged(MouseEvent arg0) {}

	@Override
	public void mouseMoved(MouseEvent arg0) {
		double width = this.renderer.getWidth();
		double height = this.renderer.getHeight();
		try {
			Image mainMenuButton = ImageIO.read(this.mainMenuFile);
			Image quitGameButton = ImageIO.read(this.quitFile);
			int x1 = (int)(width/2-mainMenuButton.getWidth(null)/2), y1 = (int)(height/2 - mainMenuButton.getHeight(null) - 50);
			int x2 = (int)(width/2-quitGameButton.getWidth(null)/2), y2 = (int)(height/2 + 50);
			int x = arg0.getX();
			int y = arg0.getY();
			if(x1<x && x < x1 + mainMenuButton.getWidth(null) && y1 < y && y < y1 + mainMenuButton.getHeight(null)) {
				this.renderer.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			} else if(x2<x && x < x2 + quitGameButton.getWidth(null) && y2 < y && y < y2 + quitGameButton.getHeight(null)) {
				this.renderer.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			} else {
				this.renderer.setCursor(Cursor.getDefaultCursor());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void paint(Graphics2D g) {
		double width = this.renderer.getWidth();
		double height = this.renderer.getHeight();
		try {
			Image mainMenuButton = ImageIO.read(this.mainMenuFile);
			Image quitGameButton = ImageIO.read(this.quitFile);
			g.drawImage(result, (int)(width/2-result.getWidth(null)/2), (int)(height/2 - result.getHeight(null) - 400), null);
			g.drawImage(mainMenuButton, (int)(width/2-mainMenuButton.getWidth(null)/2), (int)(height/2 - mainMenuButton.getHeight(null) - 50), null);
			g.drawImage(quitGameButton, (int)(width/2-quitGameButton.getWidth(null)/2), (int)(height/2 + 50), null);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
