package team7.supergoomba.userinterfaces.menus;

import java.awt.Cursor;
import java.awt.Graphics2D;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.awt.event.MouseEvent;
import javax.imageio.ImageIO;


import team7.supergoomba.Renderer2D;
import team7.supergoomba.userinterfaces.Menu;

public class MainMenu extends Menu {
	
	private final int titleOffset = 100;
	private final File titleFile = new File("./resources/menus/logo.png");
	private final File singleplayerFile = new File("./resources/menus/singleplayer.png");
	private final File multiplayerFile = new File("./resources/menus/multiplayer.png");
	private final File backgroundFile = new File("./resources/menus/background.jpg");
	private final int button1Offset = 50;
	private final int buttonSpace = 80;
	private Image background;
	private Image singleplayer;
	private Image multiplayer;
	private Image title;
	
	
	public MainMenu(Renderer2D renderer) {
		super(renderer);
		try {
			this.background = ImageIO.read(backgroundFile);
			this.multiplayer = ImageIO.read(multiplayerFile);
			this.singleplayer = ImageIO.read(singleplayerFile);
			this.title = ImageIO.read(titleFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void paint(Graphics2D g) {
		g.drawImage(background, 0, 0, renderer.getWidth(), renderer.getHeight(), null);
		int centerX = renderer.getWidth()/2;
		int centerY = renderer.getHeight()/2;
		g.drawImage(title, centerX - title.getWidth(null)/2, titleOffset, null);
		g.drawImage(singleplayer, centerX-singleplayer.getWidth(null)/2, centerY - button1Offset, null);
		g.drawImage(multiplayer, centerX-multiplayer.getWidth(null)/2, centerY - button1Offset + buttonSpace + singleplayer.getHeight(null), null);
	}

	public void onSinglePlayerClick() {
		this.choice = 1;
	}
	
	public void onMultiPlayerClick() {
		this.choice = 2;
	}
	
	@Override
	public void mouseClicked(MouseEvent arg0) {
		int centerX = renderer.getWidth()/2, centerY = renderer.getHeight()/2;
		int x = arg0.getX(), y = arg0.getY();
		int b1y1 = centerY-button1Offset, b1y2 = centerY-button1Offset+singleplayer.getHeight(null),
				b1x1 = centerX - singleplayer.getWidth(null)/2, b1x2 = centerX - singleplayer.getWidth(null)/2 + singleplayer.getWidth(null);
		int b2x1 = centerX - multiplayer.getWidth(null)/2, b2x2 = b2x1 + multiplayer.getWidth(null),
				b2y1 = b1y2 + buttonSpace, b2y2 = b2y1 + multiplayer.getHeight(null);
		if(b1x1<=x && x<=b1x2 && b1y1<=y && y<=b1y2) {
			onSinglePlayerClick();
		}
		if(b2x1<=x && x<=b2x2 && b2y1<=y && y<=b2y2) {
			onMultiPlayerClick();
		}
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {}

	@Override
	public void mouseExited(MouseEvent e) {}

	@Override
	public void mousePressed(MouseEvent e) {}

	@Override
	public void mouseReleased(MouseEvent e) {}

	@Override
	public void mouseDragged(MouseEvent arg0) {}

	@Override
	public void mouseMoved(MouseEvent arg0) {
		int centerX = renderer.getWidth()/2, centerY = renderer.getHeight()/2;
		int x = arg0.getX(), y = arg0.getY();
		int b1y1 = centerY-button1Offset, b1y2 = centerY-button1Offset+singleplayer.getHeight(null),
				b1x1 = centerX - singleplayer.getWidth(null)/2, b1x2 = centerX - singleplayer.getWidth(null)/2 + singleplayer.getWidth(null);
		int b2x1 = centerX - multiplayer.getWidth(null)/2, b2x2 = b2x1 + multiplayer.getWidth(null),
				b2y1 = b1y2 + buttonSpace, b2y2 = b2y1 + multiplayer.getHeight(null);
		if((b1x1<=x && x<=b1x2 && b1y1<=y && y<=b1y2)||(b2x1<=x && x<=b2x2 && b2y1<=y && y<=b2y2)) {
			this.renderer.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		} else {
			this.renderer.setCursor(Cursor.getDefaultCursor());
		}
	}
}
