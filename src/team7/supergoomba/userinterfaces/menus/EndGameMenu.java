package team7.supergoomba.userinterfaces.menus;

import java.awt.Graphics2D;
import java.awt.event.MouseEvent;

import team7.supergoomba.Renderer2D;
import team7.supergoomba.userinterfaces.Menu;

public class EndGameMenu extends Menu {

	int choice = -1;
	
	public EndGameMenu(Renderer2D renderer, int winner) {
		super(renderer);
	}
	
	public int getChoice() {
		return choice;
	}
	
	@Override
	public void paint(Graphics2D g) {
		
		
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {}

	@Override
	public void mouseEntered(MouseEvent e) {}

	@Override
	public void mouseExited(MouseEvent e) {}

	@Override
	public void mousePressed(MouseEvent e) {}

	@Override
	public void mouseReleased(MouseEvent e) {}

	@Override
	public void mouseDragged(MouseEvent e) {}

	@Override
	public void mouseMoved(MouseEvent e) {}
	
}
