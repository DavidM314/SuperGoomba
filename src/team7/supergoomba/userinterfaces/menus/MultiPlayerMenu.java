package team7.supergoomba.userinterfaces.menus;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import team7.supergoomba.Renderer2D;
import team7.supergoomba.objectinteractionsystems.characters.EnumCharacter;
import team7.supergoomba.userinterfaces.Menu;

public class MultiPlayerMenu extends Menu {
	
	private final File titleFile = new File("./resources/menus/logo.png");
	private Image title;
	int player;
	
	public MultiPlayerMenu(Renderer2D renderer, int player) {
		super(renderer);
		try {
			title = ImageIO.read(titleFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.player = player;
	}
	
	private Image[] getImages() {
		Image[] images = new Image[8];
		try {
			if(player==1) {
				images[0] = ImageIO.read(new File("./resources/sprites/characters/goomba/icon.png"));
				images[1] = ImageIO.read(new File("./resources/sprites/characters/koopa_troopa/icon.png"));
				images[2] = ImageIO.read(new File("./resources/sprites/characters/shy_guy/icon.png"));
				images[3] = ImageIO.read(new File("./resources/sprites/characters/spiny/icon.png"));
				images[4] = ImageIO.read(new File("./resources/sprites/characters/boo/icon.png"));
				images[5] = ImageIO.read(new File("./resources/sprites/characters/hammer_bro/icon.png"));
				images[6] = ImageIO.read(new File("./resources/sprites/characters/bowser_jr/icon.png"));
				images[7] = ImageIO.read(new File("./resources/sprites/characters/bowser/icon.png"));
			} else {
				images[0] = ImageIO.read(new File("./resources/sprites/characters/toad/icon.png"));
				images[1] = ImageIO.read(new File("./resources/sprites/characters/peach/icon.png"));
				images[2] = ImageIO.read(new File("./resources/sprites/characters/yoshi/icon.png"));
				images[3] = ImageIO.read(new File("./resources/sprites/characters/daisy/icon.png"));
				images[4] = ImageIO.read(new File("./resources/sprites/characters/diddy_kong/icon.png"));
				images[5] = ImageIO.read(new File("./resources/sprites/characters/donkey_kong/icon.png"));
				images[6] = ImageIO.read(new File("./resources/sprites/characters/luigi/icon.png"));
				images[7] = ImageIO.read(new File("./resources/sprites/characters/mario/icon.png"));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return images;
	}
	
	@Override
	public void paint(Graphics2D g) {
		double width = renderer.getWidth();
		double height = renderer.getHeight();
		g.drawImage(title, (int)(width/2-title.getWidth(null)/2), 100, null);
		g.setColor(Color.BLACK);
		int level1Y = (int) (height/3.203333333333333);
		Image[] icons = getImages();
		for(int i = 0; i<4; i++) {
			g.drawImage(icons[i], i*(int)(width/4.266666666666667) + (int)(width/21.333333333333332), level1Y, (int)(width/5.12), (int)(height/3.844), null);
			g.drawRect(i*(int)(width/4.266666666666667) + (int)(width/21.333333333333332), level1Y, (int)(width/5.12), (int)(height/3.844));
		}
		int level2Y = (int)(height/1.6016666666666666);
		for(int i = 0; i<4; i++) {
			g.drawImage(icons[i+4], i*(int)(width/4.266666666666667) + (int)(width/21.333333333333332), level2Y, (int)(width/5.12), (int)(height/3.844), null);
			g.drawRect(i*(int)(width/4.266666666666667) + (int)(width/21.333333333333332), level2Y, (int)(width/5.12), (int)(height/3.844));
		}
		Image playerNumber = null;
		try {
			playerNumber = ImageIO.read(new File("./resources/menus/player" + this.player + ".png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		g.drawImage(playerNumber, 20, 20, (int)(width/6.4), (int)(height/14.56060606060606), null);
	}
	
	@Override
	public void mouseClicked(MouseEvent arg0) {
		int x = arg0.getX(), y = arg0.getY();
		int boxClicked = -1;
		double width = renderer.getWidth();
		double height = renderer.getHeight();
		int level1Y = (int) (height/3.203333333333333);
		for(int i = 0; i<4; i++) {
			int boxX = i*(int)(width/4.266666666666667) + (int)(width/21.333333333333332);
			int boxY = level1Y;
			int boxWidth = (int)(width/5.12);
			int boxHeight = (int)(height/3.844);
			if(boxX <= x && x <= boxX + boxWidth && boxY <= y && y <= boxY + boxHeight) {
				boxClicked = i;
			}
		}
		int level2Y = (int)(height/1.6016666666666666);
		for(int i = 0; i<4; i++) {
			int boxX = i*(int)(width/4.266666666666667) + (int)(width/21.333333333333332);
			int boxY = level2Y;
			int boxWidth = (int)(width/5.12);
			int boxHeight = (int)(height/3.844);
			if(boxX <= x && x <= boxX + boxWidth && boxY <= y && y <= boxY + boxHeight) {
				boxClicked = i + 4;
			}
		}
		if(this.player==1) {
			switch(boxClicked) {
			case -1:
				break;
			case 0:
				this.choice = EnumCharacter.GOOMBA.ordinal();
				break;
			case 1:
				this.choice = EnumCharacter.KOOPA_TROOPA.ordinal();
				break;
			case 2:
				this.choice = EnumCharacter.SHY_GUY.ordinal();
				break;
			case 3:
				this.choice = EnumCharacter.SPINY.ordinal();
				break;
			case 4:
				this.choice = EnumCharacter.BOO.ordinal();
				break;
			case 5:
				this.choice = EnumCharacter.HAMMER_BRO.ordinal();
				break;
			case 6:
				this.choice = EnumCharacter.BOWSER_JR.ordinal();
				break;
			case 7:
				this.choice = EnumCharacter.BOWSER.ordinal();
				break;
			}
		} else {
			switch(boxClicked) {
			case -1:
				break;
			case 0:
				this.choice = EnumCharacter.TOAD.ordinal();
				break;
			case 1:
				this.choice = EnumCharacter.PEACH.ordinal();
				break;
			case 2:
				this.choice = EnumCharacter.YOSHI.ordinal();
				break;
			case 3:
				this.choice = EnumCharacter.DAISY.ordinal();
				break;
			case 4:
				this.choice = EnumCharacter.DIDDY_KONG.ordinal();
				break;
			case 5:
				this.choice = EnumCharacter.DONKEY_KONG.ordinal();
				break;
			case 6:
				this.choice = EnumCharacter.LUIGI.ordinal();
				break;
			case 7:
				this.choice = EnumCharacter.MARIO.ordinal();
				break;
			}
		}
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {}

	@Override
	public void mouseExited(MouseEvent arg0) {}

	@Override
	public void mousePressed(MouseEvent arg0) {}

	@Override
	public void mouseReleased(MouseEvent arg0) {}

	@Override
	public void mouseDragged(MouseEvent arg0) {}

	@Override
	public void mouseMoved(MouseEvent arg0) {
		int x = arg0.getX(), y = arg0.getY();
		int boxClicked = -1;
		double width = renderer.getWidth();
		double height = renderer.getHeight();
		int level1Y = (int) (height/3.203333333333333);
		for(int i = 0; i<4; i++) {
			int boxX = i*(int)(width/4.266666666666667) + (int)(width/21.333333333333332);
			int boxY = level1Y;
			int boxWidth = (int)(width/5.12);
			int boxHeight = (int)(height/3.844);
			if(boxX <= x && x <= boxX + boxWidth && boxY <= y && y <= boxY + boxHeight) {
				boxClicked = i;
			}
		}
		int level2Y = (int)(height/1.6016666666666666);
		for(int i = 0; i<4; i++) {
			int boxX = i*(int)(width/4.266666666666667) + (int)(width/21.333333333333332);
			int boxY = level2Y;
			int boxWidth = (int)(width/5.12);
			int boxHeight = (int)(height/3.844);
			if(boxX <= x && x <= boxX + boxWidth && boxY <= y && y <= boxY + boxHeight) {
				boxClicked = i + 4;
			}
		}
		if(boxClicked!=-1) {
			renderer.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		} else {
			renderer.setCursor(Cursor.getDefaultCursor());
		}
	}

}
