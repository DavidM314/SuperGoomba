package team7.supergoomba.userinterfaces.menus;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import team7.supergoomba.Renderer2D;
import team7.supergoomba.objectinteractionsystems.characters.EnumCharacter;
import team7.supergoomba.userinterfaces.Menu;

public class SinglePlayerMenu extends Menu {
	private Image[] characters = new Image[8];
	private Image title;
	private int tempChoice = -1;
	String cpuChoice;
	
	public SinglePlayerMenu(Renderer2D renderer) {
		super(renderer);
		try {
			title = ImageIO.read(new File("./resources/menus/logo.png"));
			characters[0] = ImageIO.read(new File("./resources/sprites/characters/goomba/still.png"));
			characters[1] = ImageIO.read(new File("./resources/sprites/characters/koopatroopa/still.png"));
		} catch (IOException e) {
			
		}
	}
	
	private Image[] getImages() {
		Image[] images = new Image[8];
		try {
			images[0] = ImageIO.read(new File("./resources/sprites/characters/goomba/icon.png"));
			images[1] = ImageIO.read(new File("./resources/sprites/characters/koopa_troopa/icon.png"));
			images[2] = ImageIO.read(new File("./resources/sprites/characters/shy_guy/icon.png"));
			images[3] = ImageIO.read(new File("./resources/sprites/characters/spiny/icon.png"));
			images[4] = ImageIO.read(new File("./resources/sprites/characters/boo/icon.png"));
			images[5] = ImageIO.read(new File("./resources/sprites/characters/hammer_bro/icon.png"));
			images[6] = ImageIO.read(new File("./resources/sprites/characters/bowser_jr/icon.png"));
			images[7] = ImageIO.read(new File("./resources/sprites/characters/bowser/icon.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return images;
	}

	public void pickCPUCharacter() {
		int boxClicked = (int)(Math.random()*8.0);
		switch(boxClicked) {
		case -1:
			break;
		case 0:
			this.tempChoice += EnumCharacter.TOAD.ordinal()*100;
			break;
		case 1:
			this.tempChoice += EnumCharacter.PEACH.ordinal()*100;
			break;
		case 2:
			this.tempChoice += EnumCharacter.YOSHI.ordinal()*100;
			break;
		case 3:
			this.tempChoice += EnumCharacter.DAISY.ordinal()*100;
			break;
		case 4:
			this.tempChoice += EnumCharacter.DIDDY_KONG.ordinal()*100;
			break;
		case 5:
			this.tempChoice += EnumCharacter.DONKEY_KONG.ordinal()*100;
			break;
		case 6:
			this.tempChoice += EnumCharacter.LUIGI.ordinal()*100;
			break;
		case 7:
			this.tempChoice += EnumCharacter.MARIO.ordinal()*100;
			break;
		}
		this.cpuChoice = "CPU: " + EnumCharacter.values()[(this.tempChoice/100)].toString().replaceAll("_", " ");
	}
	
	public void mouseClicked(MouseEvent arg0) {
		int x = arg0.getX(), y = arg0.getY();
		int boxClicked = -1;
		double width = renderer.getWidth();
		double height = renderer.getHeight();
		int level1Y = (int) (height/3.203333333333333);
		for(int i = 0; i<4; i++) {
			int boxX = i*(int)(width/4.266666666666667) + (int)(width/21.333333333333332);
			int boxY = level1Y;
			int boxWidth = (int)(width/5.12);
			int boxHeight = (int)(height/3.844);
			if(boxX <= x && x <= boxX + boxWidth && boxY <= y && y <= boxY + boxHeight) {
				boxClicked = i;
			}
		}
		int level2Y = (int)(height/1.6016666666666666);
		for(int i = 0; i<4; i++) {
			int boxX = i*(int)(width/4.266666666666667) + (int)(width/21.333333333333332);
			int boxY = level2Y;
			int boxWidth = (int)(width/5.12);
			int boxHeight = (int)(height/3.844);
			if(boxX <= x && x <= boxX + boxWidth && boxY <= y && y <= boxY + boxHeight) {
				boxClicked = i + 4;
			}
		}
		switch(boxClicked) {
		case -1:
			break;
		case 0:
			this.tempChoice = EnumCharacter.GOOMBA.ordinal();
			break;
		case 1:
			this.tempChoice = EnumCharacter.KOOPA_TROOPA.ordinal();
			break;
		case 2:
			this.tempChoice = EnumCharacter.SHY_GUY.ordinal();
			break;
		case 3:
			this.tempChoice = EnumCharacter.SPINY.ordinal();
			break;
		case 4:
			this.tempChoice = EnumCharacter.BOO.ordinal();
			break;
		case 5:
			this.tempChoice = EnumCharacter.HAMMER_BRO.ordinal();
			break;
		case 6:
			this.tempChoice = EnumCharacter.BOWSER_JR.ordinal();
			break;
		case 7:
			this.tempChoice = EnumCharacter.BOWSER.ordinal();
			break;
		}
		this.pickCPUCharacter();
		new Thread() {
			public void run() {
				try {
					Thread.sleep(3000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				SinglePlayerMenu.this.choice = SinglePlayerMenu.this.tempChoice;
			}
		}.start();
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {}

	@Override
	public void mouseExited(MouseEvent arg0) {}

	@Override
	public void mousePressed(MouseEvent arg0) {}

	@Override
	public void mouseReleased(MouseEvent arg0) {}

	public void mouseDragged(MouseEvent arg0) {}

	public void mouseMoved(MouseEvent arg0) {
		int x = arg0.getX(), y = arg0.getY();
		int boxClicked = -1;
		double width = renderer.getWidth();
		double height = renderer.getHeight();
		int level1Y = (int) (height/3.203333333333333);
		for(int i = 0; i<4; i++) {
			int boxX = i*(int)(width/4.266666666666667) + (int)(width/21.333333333333332);
			int boxY = level1Y;
			int boxWidth = (int)(width/5.12);
			int boxHeight = (int)(height/3.844);
			if(boxX <= x && x <= boxX + boxWidth && boxY <= y && y <= boxY + boxHeight) {
				boxClicked = i;
			}
		}
		int level2Y = (int)(height/1.6016666666666666);
		for(int i = 0; i<4; i++) {
			int boxX = i*(int)(width/4.266666666666667) + (int)(width/21.333333333333332);
			int boxY = level2Y;
			int boxWidth = (int)(width/5.12);
			int boxHeight = (int)(height/3.844);
			if(boxX <= x && x <= boxX + boxWidth && boxY <= y && y <= boxY + boxHeight) {
				boxClicked = i + 4;
			}
		}
		if(boxClicked!=-1) {
			renderer.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		} else {
			renderer.setCursor(Cursor.getDefaultCursor());
		}
	}

	@Override
	public void paint(Graphics2D g) {
		double width = renderer.getWidth();
		double height = renderer.getHeight();
		g.drawImage(title, (int)(width/2-title.getWidth(null)/2), 100, null);
		g.setColor(Color.BLACK);
		int level1Y = (int) (height/3.203333333333333);
		Image[] icons = getImages();
		for(int i = 0; i<4; i++) {
			g.drawImage(icons[i], i*(int)(width/4.266666666666667) + (int)(width/21.333333333333332), level1Y, (int)(width/5.12), (int)(height/3.844), null);
			g.drawRect(i*(int)(width/4.266666666666667) + (int)(width/21.333333333333332), level1Y, (int)(width/5.12), (int)(height/3.844));
		}
		int level2Y = (int)(height/1.6016666666666666);
		for(int i = 0; i<4; i++) {
			g.drawImage(icons[i+4], i*(int)(width/4.266666666666667) + (int)(width/21.333333333333332), level2Y, (int)(width/5.12), (int)(height/3.844), null);
			g.drawRect(i*(int)(width/4.266666666666667) + (int)(width/21.333333333333332), level2Y, (int)(width/5.12), (int)(height/3.844));
		}
		if(this.cpuChoice!=null) {
			g.setFont(new Font("Comic Sans MS", Font.PLAIN, (int)(height/32.03333333333333)));
			g.drawString(this.cpuChoice, this.renderer.getWidth()-g.getFontMetrics().stringWidth(this.cpuChoice) - 50, 50);
		}
	}
}
