package team7.supergoomba.userinterfaces.huds;

import java.awt.Graphics2D;

import team7.supergoomba.Game;
import team7.supergoomba.Renderer2D;
import team7.supergoomba.userinterfaces.HUD;

@Deprecated
public class CharacterNameHUD extends HUD {
	
	public CharacterNameHUD(Renderer2D renderer, Game game) {
		this.renderer = renderer;
	}
	
	@Override
	public void paint(Graphics2D g) {}

}