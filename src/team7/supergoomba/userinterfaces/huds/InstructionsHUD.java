package team7.supergoomba.userinterfaces.huds;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import team7.supergoomba.Renderer2D;
import team7.supergoomba.userinterfaces.HUD;

public class InstructionsHUD extends HUD implements KeyListener {
	private boolean pressed;
	private Image instructions;
	private final File instructionsFile = new File("./resources/user interfaces/instructions.png");
	
	public InstructionsHUD(Renderer2D renderer) {
		this.renderer = renderer;
		this.pressed = false;
		try {
			instructions = ImageIO.read(instructionsFile);
		} catch (IOException e) {
			
		}
	}
	
	@Override
	public void paint(Graphics2D g) {
		g.setFont(new Font("Comic Sans MS", Font.PLAIN, (int)(renderer.getHeight()/32.03333333333333)));
		g.setColor(Color.ORANGE);
		g.drawString("Press \"I\" for Instructions", 5, 50);
		if(pressed) {
			g.drawImage(instructions, 0, 0, renderer.getWidth() - 70, renderer.getHeight() - 100, null);
		}
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if(e.getKeyCode() == KeyEvent.VK_I) {
			pressed = (pressed==false)?true:false;
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {}

	@Override
	public void keyTyped(KeyEvent e) {}

}