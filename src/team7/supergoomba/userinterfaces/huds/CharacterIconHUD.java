package team7.supergoomba.userinterfaces.huds;

import java.awt.Graphics2D;
import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import team7.supergoomba.Game;
import team7.supergoomba.Renderer2D;
import team7.supergoomba.userinterfaces.HUD;

@Deprecated
public class CharacterIconHUD extends HUD {
	private Image player1Icon;
	private Image player2Icon;
	
	public CharacterIconHUD(Renderer2D renderer, Game game) {
		this.renderer = renderer;
		try {
			player1Icon = ImageIO.read(new File(game.getPlayer(1).getSprites().getPath() + "\\icon.png"));
			player2Icon = ImageIO.read(new File(game.getPlayer(2).getSprites().getPath() + "\\icon.png"));
		} catch (IOException e) {
			
		}
	}
	
	@Override
	public void paint(Graphics2D g) {
		g.drawImage(player1Icon, 10, renderer.getHeight()-50, 40, 40, null);
		g.drawImage(player2Icon, renderer.getWidth()-50, renderer.getHeight()-50, 40, 40, null);
	}

}