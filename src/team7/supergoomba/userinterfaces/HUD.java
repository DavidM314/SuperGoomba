package team7.supergoomba.userinterfaces;

import java.awt.Graphics2D;

import team7.supergoomba.Renderer2D;
import team7.supergoomba.renderer2d.PaintableObject;

public abstract class HUD implements PaintableObject {
	public abstract void paint(Graphics2D g);
	protected Renderer2D renderer;
}
