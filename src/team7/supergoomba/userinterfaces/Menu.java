package team7.supergoomba.userinterfaces;

import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import team7.supergoomba.Renderer2D;
import team7.supergoomba.renderer2d.PaintableObject;

public abstract class Menu implements MouseListener, MouseMotionListener, PaintableObject {
	protected Renderer2D renderer;
	protected int choice;
	
	public int displayMenu() {
		renderer.addObjectToPaintQueue(this);
		while(this.getChoice()==-1) {
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		renderer.clearPaintQueue();
		return this.getChoice();
	}
	
	public int getChoice() {
		return choice;
	}
	
	public Menu(Renderer2D renderer) {
		this.renderer = renderer;
		choice = -1;
	}
	
}
