package team7.supergoomba.utils;

import java.awt.Image;

import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import team7.supergoomba.objectinteractionsystems.InGameObject;

public class Sprite {
	private double width;
	private double height;
	private Image img;
	
	public Sprite(File f) {
		try {
			img = ImageIO.read(f);
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("could not load sprite: " + f.getPath());
			System.exit(2);
		}
		this.width = img.getWidth(null);
		this.height = img.getHeight(null);
	}
	
	public Image getImg() {
		return this.img;
	}
	
	public void changeDimensions(InGameObject c) {
		double newWidth = this.width*c.getHeight()/height;
		double midX = c.getWidth()/2.0 + c.getX();
		c.setWidth(newWidth);
		c.setX(midX - newWidth/2.0);
		if(c.getX()<0) {
			c.setX(0);
		} else if (c.getX()>c.getObjectInteractionSystem().getRenderer().getWidth()) {
			c.setX(c.getObjectInteractionSystem().getRenderer().getWidth()-c.getWidth());
		}
	}
}
