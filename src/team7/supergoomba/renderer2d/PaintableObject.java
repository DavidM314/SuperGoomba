package team7.supergoomba.renderer2d;

import java.awt.Graphics2D;

public interface PaintableObject {
	public abstract void paint(Graphics2D g);
}
