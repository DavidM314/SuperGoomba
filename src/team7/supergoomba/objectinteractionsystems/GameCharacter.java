package team7.supergoomba.objectinteractionsystems;

import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

import team7.supergoomba.AudioPlayer;
import team7.supergoomba.ObjectInteractionSystem;
import team7.supergoomba.utils.Sprite;

public abstract class GameCharacter extends InGameObject implements KeyListener {
	private String name;
	private int health;
	private int maxHealth;
	private int power;
	private double speed;
	private File sprites;
	private KeyControlEnum controls;
	private boolean[] keyArray;
	private double jumpSpeed;
	private GameCharacter enemy;
	private double maxSpeed;
	private boolean isBot;
	private long lastAttack;
	private Stack<CharacterState> newstate;
	private EnumDirection direction;
	private long lastDamageDealt;
	protected long lastSpecialAttack;
	protected long lastSuperSpecialAttack;
	private AudioPlayer audioPlayer;
	protected final long attackDelay;
	protected final long specialAttackDelay;
	protected final long superSpecialAttackDelay;
	private boolean botAttackMode;
	private Map<String,Sprite> spritesMap;
	
	@Deprecated
	public abstract GameCharacter clone();
	public abstract void specialAttack();
	public abstract void superSpecialAttack();
	public abstract void onCollision(GameCharacter g);
	
	public GameCharacter(double x, double y, double width, double height, ObjectInteractionSystem oiSystem,
			String name, int maxHealth, int power, double speed, File sprites, KeyControlEnum controls, boolean isBot,
			long specialAttackDelay, long superSpecialAttackDelay) {
		super(x,y,width,height,0,0,true,oiSystem);
		this.name = name;
		this.maxHealth = maxHealth;
		this.power = power;
		this.speed = speed;
		this.sprites = sprites;
		this.controls = controls;
		this.isBot = isBot;
		this.health = maxHealth;
		this.keyArray = new boolean[4];
		this.jumpSpeed = 50;
		this.maxSpeed = speed*3.0;
		this.lastAttack = System.currentTimeMillis();
		this.newstate = new Stack<CharacterState>() {
			private static final long serialVersionUID = -1129892179816320573L;

			@Override
			public CharacterState peek() {
				if(this.isEmpty()) {
					return null;
				}
				return super.peek();
			}
		};
		this.newstate.push(CharacterState.STILL);
		this.direction = EnumDirection.RIGHT;
		this.lastDamageDealt = 0;
		this.lastSpecialAttack = 0;
		this.lastSuperSpecialAttack = System.currentTimeMillis();
		this.attackDelay = 700;
		this.specialAttackDelay = specialAttackDelay;
		this.superSpecialAttackDelay = superSpecialAttackDelay;
		this.spritesMap = new HashMap<String,Sprite>();
		this.audioPlayer = new AudioPlayer();
		this.botAttackMode = false;
	}

	@Deprecated
	public GameCharacter(GameCharacter c) {
		super(c);
		this.name = c.name;
		this.health = c.health;
		this.maxHealth = c.maxHealth;
		this.power = c.power;
		this.speed = c.speed;
		this.sprites = c.sprites;
		this.controls = c.controls;
		this.keyArray = c.keyArray;
		this.jumpSpeed = c.jumpSpeed;
		this.enemy = c.enemy;
		this.maxSpeed = c.maxSpeed;
		this.isBot = c.isBot;
		this.lastAttack = c.lastAttack;
		this.superSpecialAttackDelay = c.superSpecialAttackDelay;
		this.specialAttackDelay = c.specialAttackDelay;
		this.attackDelay = c.attackDelay;
	}
	
	@Override
	public void paint(Graphics2D g) {
		if(this.isBot) {
			botProcesses();
		}
		if(this.getMotionX()!=0) { 
			if(this.getMotionX()<0 && this.getMotionX()>-5 && keyArray[0]) { //minimum speed left
				this.setMotionX(-5.0);
			} else if (this.getMotionX()>0 && this.getMotionX()<5 && keyArray[2]) { //minimum speed right
				this.setMotionX(5.0);
			}
		}
		if((keyArray[0] || keyArray[2]) && this.getStateStack().peek()==CharacterState.STILL) {
			this.getStateStack().push(CharacterState.MOVING);
		}
		if(this.getStateStack().peek()==CharacterState.MOVING && (!keyArray[0] && !keyArray[2])) {
			this.getStateStack().pop();
		}
		if(keyArray[0] && this.getMotionX()>-maxSpeed) {
			this.changeMotionX(-speed);
		}
		if(keyArray[2] && this.getMotionX()<maxSpeed) {
			this.changeMotionX(speed);
		}
		
		if (keyArray[1] && !this.getGravityAffected()) {
			this.changeMotionY(10);
		}
		if (keyArray[3] && !this.getGravityAffected()) {
			this.changeMotionY(-10);
		}
		Sprite s;
		switch(newstate.peek()) {
		case STILL:
			s = this.getSprite(sprites.getPath() + "\\still" + direction.ordinal() + ".png");
			break;
		case ATTACKING:
			s = this.getSprite(sprites.getPath() + "\\attacking" + direction.ordinal() + ".png");
			break;
		case JUMPING:
			s = this.getSprite(sprites.getPath() + "\\jumping" + direction.ordinal() + ".png");
			break;
		case MOVING:
			s = this.getSprite(sprites.getPath() + "\\moving" + direction.ordinal() + ".png");
			break;
		case SPECIAL_ATTACKING:
			s = this.getSprite(sprites.getPath() + "\\special_attacking" + direction.ordinal() + ".png");
			break;
		case S_SPECIAL_ATTACKING:
			s = this.getSprite(sprites.getPath() + "\\s_special_attacking" + direction.ordinal() + ".png");
			break;
		default:
			throw new RuntimeException();
		}
		this.update();
		s.changeDimensions(this);
		g.drawImage(s.getImg(), (int)this.getX(), (int)this.getY(), (int)this.getWidth(), (int)this.getHeight(), null);
	}
	
	public void update() {
		this.setX(this.getX() + this.getMotionX());
		this.setY(this.getY() - this.getMotionY());
	}
	
	@Override
	public void keyPressed(KeyEvent e) {
		if(controls==KeyControlEnum.ARROW_KEYS) {
			switch(e.getKeyCode()) {
			case KeyEvent.VK_LEFT:
				this.keyArray[0] = true;
				this.direction = EnumDirection.LEFT;
				break;
			case KeyEvent.VK_UP:
				if(this.getOnGround() && !this.keyArray[1] && this.getGravityAffected()) {
					jump();
				}
				this.keyArray[1] = true;
				break;
			case KeyEvent.VK_RIGHT:
				this.keyArray[2] = true;
				this.direction = EnumDirection.RIGHT;
				break;
			case KeyEvent.VK_DOWN:
				this.keyArray[3] = true;
				break;
			case KeyEvent.VK_NUMPAD1:
				attack();
				break;
			case KeyEvent.VK_NUMPAD2:
				specialAttack();
				break;
			case KeyEvent.VK_NUMPAD3:
				superSpecialAttack();
				break;
			}
		} else if (controls==KeyControlEnum.WASD_KEYS) {
			switch(e.getKeyCode()) {
			case KeyEvent.VK_A:
				this.keyArray[0] = true;
				this.direction = EnumDirection.LEFT;
				break;
			case KeyEvent.VK_W:
				if(this.getOnGround() && !this.keyArray[1] && this.getGravityAffected()) {
					jump();
				}
				this.keyArray[1] = true;
				break;
			case KeyEvent.VK_D:
				this.keyArray[2] = true;
				this.direction = EnumDirection.RIGHT;
				break;
			case KeyEvent.VK_S:
				this.keyArray[3] = true;
				break;
			case KeyEvent.VK_J:
				attack();
				break;
			case KeyEvent.VK_K:
				specialAttack();
				break;
			case KeyEvent.VK_L:
				superSpecialAttack();
				break;
			}
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		if(controls==KeyControlEnum.ARROW_KEYS) {
			switch(e.getKeyCode()) {
			case KeyEvent.VK_LEFT:
				this.keyArray[0] = false;
				if(this.keyArray[2]) {
					this.direction = EnumDirection.RIGHT;
				}
				break;
			case KeyEvent.VK_UP:
				this.keyArray[1] = false;
				break;
			case KeyEvent.VK_RIGHT:
				this.keyArray[2] = false;
				if(this.keyArray[0]) {
					this.direction = EnumDirection.LEFT;
				}
				break;
			case KeyEvent.VK_DOWN:
				this.keyArray[3] = false;
				break;
			}
		} else if (controls==KeyControlEnum.WASD_KEYS) {
			switch(e.getKeyCode()) {
			case KeyEvent.VK_A:
				this.keyArray[0] = false;
				if(this.keyArray[2]) {
					this.direction = EnumDirection.RIGHT;
				}
				break;
			case KeyEvent.VK_W:
				this.keyArray[1] = false;
				break;
			case KeyEvent.VK_D:
				this.keyArray[2] = false;
				if(this.keyArray[0]) {
					this.direction = EnumDirection.LEFT;
				}
				break;
			case KeyEvent.VK_S:
				this.keyArray[3] = false;
				break;
			}			
		}		
	}

	public void reduceHP(int amount, InGameObject o) {
		this.health -= amount;
	}
	
	@SuppressWarnings("static-access")
	public void attack() {
		if(System.currentTimeMillis()-lastAttack<this.attackDelay || this.newstate.peek() == CharacterState.SPECIAL_ATTACKING || this.newstate.peek() == CharacterState.S_SPECIAL_ATTACKING) {
			return;
		}
		audioPlayer.getSoundEffect().ATTACK.play();
		lastAttack = System.currentTimeMillis();
		newstate.push(CharacterState.ATTACKING);
		new Thread() {
			public void run() {
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if(GameCharacter.this.getStateStack().peek()==CharacterState.ATTACKING) {
					GameCharacter.this.getStateStack().pop();
				}
			}
		}.start();
	}

	@SuppressWarnings("static-access")
	public void jump() {
		audioPlayer.getSoundEffect().JUMP.play();
		this.setY(this.getY() - 4.0);
		this.setOnGround(false);
		this.setMotionY(this.jumpSpeed);
		Stack<CharacterState> state = this.getStateStack();
		Stack<CharacterState> temp =  new Stack<CharacterState>() {
			private static final long serialVersionUID = -7162866808471340007L;

			@Override
			public CharacterState peek() {
				if(this.isEmpty()) {
					return null;
				}
				return super.peek();
			}
		};
		while(state.peek()!=CharacterState.STILL && state.peek()!=CharacterState.MOVING) {
			temp.push(state.pop());
		}
		state.push(CharacterState.JUMPING);
		while(temp.peek()!=null) {
			state.push(temp.pop());
		}
	}
	
	public void botProcesses() {
		CharacterState enemyState = enemy.getStateStack().peek();
		CharacterState myState = this.getStateStack().peek();
		boolean lookingToAttack = this.botAttackMode || 
				(0==(int)Math.random()*25.0) ||
				(myState==CharacterState.ATTACKING ||
				myState==CharacterState.SPECIAL_ATTACKING ||
				myState==CharacterState.S_SPECIAL_ATTACKING) ||
				(enemyState!=CharacterState.ATTACKING &&
				enemyState!=CharacterState.SPECIAL_ATTACKING &&
				enemyState!=CharacterState.S_SPECIAL_ATTACKING && 
				System.currentTimeMillis()-this.lastAttack>this.attackDelay &&
				System.currentTimeMillis()-this.lastSpecialAttack>this.specialAttackDelay &&
				System.currentTimeMillis()-this.lastSuperSpecialAttack>this.superSpecialAttackDelay);
		double x = enemy.getX()-enemy.getMotionX()*15.0;
		double y = enemy.getY()-enemy.getMotionY()*15.0;
		if(lookingToAttack) {
			this.botAttackMode = true;
			if(x<this.getX()) {
				this.direction = EnumDirection.LEFT;
				keyArray[0] = true;
				keyArray[2] = false;
			} else if (x>this.getX()) {
				this.direction = EnumDirection.RIGHT;
				keyArray[2] = true;
				keyArray[0] = false;
			}
			if(Math.abs(this.getX()-x)<100 && y<this.getY()) {
				if(this.getOnGround()) {
					jump();
				}
				keyArray[1] = true;
				keyArray[3] = false;
			} else if (Math.abs(this.getX()-x)<200 && y>this.getY()) {
				keyArray[3] = true;
				keyArray[1] = false;
			}
			if(Math.sqrt(Math.pow(this.getX()-x, 2) + Math.pow(this.getY()-y, 2))<200) {
				if(System.currentTimeMillis()-this.lastSuperSpecialAttack>this.superSpecialAttackDelay) {
					this.superSpecialAttack();
				} else if (System.currentTimeMillis()-this.lastSpecialAttack>this.specialAttackDelay) {
					this.specialAttack();
				} else if (System.currentTimeMillis()-this.lastAttack>this.attackDelay) {
					this.attack();
				}
			}
		} else {
			this.botAttackMode = false;
			if(x<this.getX()) {
				this.direction = EnumDirection.LEFT;
				keyArray[0] = true;
				keyArray[2] = false;
			} else if (x>=this.getX()) {
				this.direction = EnumDirection.RIGHT;
				keyArray[2] = true;
				keyArray[0] = false;
			}
			if(this.getOnGround() && (int)(Math.random()*100.0)==0) {
				jump();
			}
		}
	}
	
	
	
	public long getSuperSpecialAttackDelay() {
		return this.superSpecialAttackDelay;
	}
	
	public long getLastSuperSpecialAttack() {
		return this.lastSuperSpecialAttack;
	}
	
	public void setBotAttackMode(boolean b) {
		this.botAttackMode = b;
	}
	
	public KeyControlEnum getControls() {
		return this.controls;
	}

	public long getLastDamageDealt() {
		return this.lastDamageDealt;
	}
	
	public void setLastDamageDealt(long time) {
		this.lastDamageDealt = time;
	}
	
	public int getMaxHealth() {
		return this.maxHealth;
	}
		
	public Sprite getSprite(String fileName) {
		if(spritesMap.containsKey(fileName)) {
			return spritesMap.get(fileName);
		}
		return new Sprite(new File(fileName));
	}
	
	public double getMaxSpeed() {
		return this.maxSpeed;
	}
	
	public void setMaxSpeed(double d) {
		this.maxSpeed = d;
	}
	
	public void setBot(boolean state) {
		this.isBot = state;
	}

	public double getPercentHealth() {
		return ((double)(health))/((double)(maxHealth));
	}
	
	public String getName() {
		return name;
	}
	
	public boolean getBot() {
		return this.isBot;
	}
	
	public Stack<CharacterState> getStateStack() {
		return this.newstate;
	}
	
	public int getHealth() {
		return this.health;
	}
	
	public File getSprites() {
		return this.sprites;
	}
	
	public void setEnemy(GameCharacter enemy) {
		this.enemy = enemy;
	}
	
	public EnumDirection getDirection() {
		return this.direction;
	}
	
	public long getLastAttack() {
		return this.lastAttack;
	}
	
	public void setLastAttack(long lastAttack) {
		this.lastAttack = lastAttack;
	}
	
	public void setDirection(EnumDirection d) {
		this.direction = d;
	}

	public int getPower() {
		return this.power;
	}

	public boolean[] getKeyArray() {
		return this.keyArray;
	}
	
	public void setKeyArray(boolean[] bArray) {
		this.keyArray = bArray;
		
	}
	
	@Override
	public void keyTyped(KeyEvent e) {}
	
}
