package team7.supergoomba.objectinteractionsystems;

import team7.supergoomba.ObjectInteractionSystem;
import team7.supergoomba.renderer2d.PaintableObject;

public abstract class InGameObject implements PaintableObject {
	private double x;
	private double y;
	private double width;
	private double height;
	private double motionX;
	private double motionY;
	private boolean gravityAffected;
	private boolean onGround;
	private ObjectInteractionSystem oiSystem;
	
	public void onCollision(InGameObject o) {}
	
	@Deprecated
	public InGameObject(InGameObject o) {
		this.x = o.x;
		this.y = o.y;
		this.width = o.width;
		this.height = o.height;
		this.motionX = o.motionX;
		this.motionY = o.motionY;
		this.gravityAffected = o.gravityAffected;
		this.onGround = o.onGround;
		this.oiSystem = o.oiSystem;
	}
	
	public InGameObject(double x, double y, double width, double height, double motionX, double motionY, boolean gravityAffected, ObjectInteractionSystem oiSystem) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.motionX = motionX;
		this.motionY = motionY;
		this.gravityAffected = gravityAffected;
		this.onGround = oiSystem.isOnGround(this);
		this.oiSystem = oiSystem;
	}
	
	public boolean getGravityAffected() {
		return gravityAffected;
	}
	
	public void setGravityAffected(boolean b) {
		this.gravityAffected = b;
	}
	
	public boolean getOnGround() {
		return onGround;
	}
	
	public void setOnGround(boolean b) {
		onGround = b;
	}
	
	public ObjectInteractionSystem getObjectInteractionSystem() {
		return oiSystem;
	}
	
	public void setObjectInteractionSystem(ObjectInteractionSystem oiSystem) {
		this.oiSystem = oiSystem;
	}
	
	public void setX(double x) {
		this.x = x;
	}
	
	public void setY(double y) {
		this.y = y;
	}
	
	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}
	
	public double getWidth() {
		return width;
	}
	
	public double getHeight() {
		return height;
	}
	
	public void changeMotionX(double amount) {
		this.motionX += amount;
	}
	
	public void changeMotionY(double amount) {
		this.motionY += amount;
	}
	
	public void setWidth(double width) {
		this.width = width;
	}
	
	public void setHeight(double height) {
		this.height = height;
	}
	
	public double getMotionX() {
		return motionX;
	}
	
	public double getMotionY() {
		return motionY;
	}

	public void setMotionX(double d) {
		this.motionX = d;
	}
	
	public void setMotionY(double d) {
		this.motionY = d;
	}
}
