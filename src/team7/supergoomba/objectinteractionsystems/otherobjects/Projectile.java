package team7.supergoomba.objectinteractionsystems.otherobjects;

import team7.supergoomba.objectinteractionsystems.GameCharacter;

public interface Projectile {
	public abstract boolean shouldRemove();
	public abstract void onCollision(GameCharacter g);
}
