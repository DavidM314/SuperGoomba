package team7.supergoomba.objectinteractionsystems.otherobjects;

import java.awt.Graphics2D;
import java.io.File;

import team7.supergoomba.ObjectInteractionSystem;
import team7.supergoomba.objectinteractionsystems.EnumDirection;
import team7.supergoomba.objectinteractionsystems.GameCharacter;
import team7.supergoomba.objectinteractionsystems.InGameObject;
import team7.supergoomba.utils.Sprite;

public class Barrel extends InGameObject implements Projectile {

	private long creationTime;
	private final long maxTime = 10000;
	private GameCharacter immune;
	private boolean exploded;
	Sprite[] sprites;
	int spriteState;
	int dmg;
	
	public Barrel(ObjectInteractionSystem oiSystem, GameCharacter player, double scale) {
		super(player.getDirection()==EnumDirection.RIGHT?(player.getX() + player.getWidth()):(player.getX() - 100.0*scale), player.getY() - 100.0*scale, 100.0*scale, 100.0*scale, player.getDirection()==EnumDirection.RIGHT?60:-60, 20, true, oiSystem);
		this.creationTime = System.currentTimeMillis();
		this.immune = player;
		this.exploded = false;
		sprites = new Sprite[4];
		for(int i = 1; i<=4; i++) {
			sprites[i-1] = new Sprite(new File("./resources/sprites/others/barrel/barrel" + i + ".png"));
		}
		spriteState = 0;
		this.dmg = (int)(10.0*scale);
	}
	
	public Barrel(ObjectInteractionSystem oiSystem, GameCharacter player) {
		super(player.getDirection()==EnumDirection.RIGHT?(player.getX() + player.getWidth()):(player.getX() - 100), player.getY() - 100, 100, 100, player.getDirection()==EnumDirection.RIGHT?60:-60, 20, true, oiSystem);
		this.creationTime = System.currentTimeMillis();
		this.immune = player;
		this.exploded = false;
		sprites = new Sprite[4];
		for(int i = 1; i<=4; i++) {
			sprites[i-1] = new Sprite(new File("./resources/sprites/others/barrel/barrel" + i + ".png"));
		}
		spriteState = 0;
		this.dmg = 10;
	}

	private void update() {
		this.setX(this.getX() + this.getMotionX());
		this.setY(this.getY() - this.getMotionY());
		if(System.currentTimeMillis()-creationTime>maxTime) {
			Explosion e = new Explosion(this.getX(), this.getY(), this.getObjectInteractionSystem(), immune, this.dmg);
			this.getObjectInteractionSystem().add(e);
			this.exploded = true;
		}
	}
	
	public void onCollision(GameCharacter c) {
		if(c.equals(immune)) {
			return;
		}
		Explosion e = new Explosion(this.getX(), this.getY(), this.getObjectInteractionSystem(), immune, this.dmg);
		this.getObjectInteractionSystem().add(e);
		this.exploded = true;
	}
	
	public boolean shouldRemove() {
		return this.exploded;
	}
	
	@Override
	public void paint(Graphics2D g) {
		update();
		if(!this.exploded) {
			sprites[spriteState].changeDimensions(this);
			g.drawImage(sprites[spriteState].getImg(), (int)this.getX(), (int)this.getY(), (int)this.getWidth(), (int)this.getHeight(), null);
			spriteState = (spriteState+1)%4;
		}
	}
	
}
