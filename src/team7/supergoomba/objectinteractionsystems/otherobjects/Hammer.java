package team7.supergoomba.objectinteractionsystems.otherobjects;

import java.awt.Graphics2D;
import java.io.File;

import team7.supergoomba.ObjectInteractionSystem;
import team7.supergoomba.objectinteractionsystems.EnumDirection;
import team7.supergoomba.objectinteractionsystems.GameCharacter;
import team7.supergoomba.objectinteractionsystems.InGameObject;
import team7.supergoomba.utils.Sprite;

public class Hammer extends InGameObject implements Projectile {

	private long creationTime;
	private final long maxTime = 10000;
	private GameCharacter immune;
	private boolean exploded;
	Sprite[] sprites;
	int spriteState;
	int dmg;
	boolean special;
	
	public Hammer(ObjectInteractionSystem oiSystem, GameCharacter player, boolean special) {
		super(player.getDirection()==EnumDirection.RIGHT?(player.getX() + player.getWidth()):(player.getX() - 100), player.getY() - 100, 100, 100, player.getDirection()==EnumDirection.RIGHT?60:-60, 30, true, oiSystem);
		this.creationTime = System.currentTimeMillis();
		this.immune = player;
		this.exploded = false;
		sprites = new Sprite[4];
		for(int i = 1; i<=4; i++) {
			sprites[i-1] = new Sprite(new File("./resources/sprites/others/hammer/hammer" + i + ".png"));
		}
		spriteState = 0;
		this.dmg = special?10:3;
		this.special = special;
	}

	private void update() {
		this.setX(this.getX() + this.getMotionX());
		this.setY(this.getY() - this.getMotionY());
		if(System.currentTimeMillis()-creationTime>maxTime) {
			if(special) {
				Explosion e = new Explosion(this.getX(), this.getY(), this.getObjectInteractionSystem(), immune, this.dmg);
				this.getObjectInteractionSystem().add(e);
			}
			this.exploded = true;
		}
		if(this.getOnGround()) {
			if(special) {
				Explosion e = new Explosion(this.getX(), this.getY(), this.getObjectInteractionSystem(), immune, this.dmg);
				this.getObjectInteractionSystem().add(e);
			}
			this.exploded = true;
		}
	}
	
	public void onCollision(GameCharacter c) {
		if(c.equals(immune)) {
			return;
		}
		if(special) {
			Explosion e = new Explosion(this.getX(), this.getY(), this.getObjectInteractionSystem(), immune, this.dmg);
			this.getObjectInteractionSystem().add(e);
		} else {
			c.reduceHP(dmg, this);
		}
		this.exploded = true;
	}
	
	public boolean shouldRemove() {
		return this.exploded;
	}
	
	@Override
	public void paint(Graphics2D g) {
		update();
		if(!this.exploded) {
			sprites[spriteState].changeDimensions(this);
			g.drawImage(sprites[spriteState].getImg(), (int)this.getX(), (int)this.getY(), (int)this.getWidth(), (int)this.getHeight(), null);
			spriteState = (spriteState+1)%4;
		}
	}
	
}