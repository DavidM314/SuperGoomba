package team7.supergoomba.objectinteractionsystems.otherobjects;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.ArrayList;

import team7.supergoomba.ObjectInteractionSystem;
import team7.supergoomba.objectinteractionsystems.EnumDirection;
import team7.supergoomba.objectinteractionsystems.GameCharacter;
import team7.supergoomba.objectinteractionsystems.InGameObject;

public class Laser extends InGameObject implements Projectile {
	
	private long timeCreated;
	private static final long maxTime = 5000;
	private static final double motionX = 20;
	private ArrayList<GameCharacter> immune;
	private static final int damage = 7;
	
	public boolean shouldRemove() {
		return System.currentTimeMillis()-timeCreated>maxTime;
	}
	
	public void onCollision(GameCharacter c) {
		if(immune.contains(c)) {
			return;
		}
		c.setMotionY(-c.getMotionY());
		c.setMotionX(-c.getMotionX());
		c.reduceHP(damage,this);
		immune.add(c);
	}
	
	public Laser(GameCharacter c,
			ObjectInteractionSystem oiSystem) {
		super(c.getDirection()==EnumDirection.LEFT?c.getX()-100-c.getWidth():c.getX() + c.getWidth(), c.getY() + c.getHeight()/3, 100, 2, c.getDirection()==EnumDirection.LEFT?-motionX:motionX, 0, false, oiSystem);
		this.timeCreated = System.currentTimeMillis();
		this.immune = new ArrayList<GameCharacter>();
		this.immune.add(c);
	}

	@Override
	public void paint(Graphics2D g) {
		update();
		g.setColor(Color.GREEN);
		g.fillRoundRect((int)this.getX(), (int)this.getY(), (int)this.getWidth(), (int)this.getHeight(), 2, 2);
	}
	
	public void update() {
		this.setX(this.getX() + this.getMotionX());
	}
}
