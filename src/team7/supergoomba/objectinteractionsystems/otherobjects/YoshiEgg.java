package team7.supergoomba.objectinteractionsystems.otherobjects;

import java.awt.Graphics2D;
import java.io.File;

import team7.supergoomba.ObjectInteractionSystem;
import team7.supergoomba.objectinteractionsystems.GameCharacter;
import team7.supergoomba.objectinteractionsystems.InGameObject;
import team7.supergoomba.utils.Sprite;

public class YoshiEgg extends InGameObject implements Projectile {

	private long creationTime;
	private final long maxTime = 10000;
	private GameCharacter immune;
	private boolean exploded;
	Sprite eggSprite = new Sprite(new File("./resources/sprites/others/yoshi_egg.png"));
		
	public YoshiEgg(double x, double y, ObjectInteractionSystem oiSystem, GameCharacter player) {
		super(x, y, 100, 100, 0, 0, true, oiSystem);
		this.creationTime = System.currentTimeMillis();
		this.immune = player;
		this.exploded = false;
	}

	private void update() {
		if(System.currentTimeMillis()-creationTime>maxTime) {
			Explosion e = new Explosion(this.getX(), this.getY(), this.getObjectInteractionSystem(), immune, 10);
			this.getObjectInteractionSystem().add(e);
			this.exploded = true;
		}
	}
	
	public void onCollision(GameCharacter c) {
		if(c.equals(immune)) {
			return;
		}
		Explosion e = new Explosion(this.getX(), this.getY(), this.getObjectInteractionSystem(), immune, 10);
		this.getObjectInteractionSystem().add(e);
		this.exploded = true;
	}
	
	public boolean shouldRemove() {
		return this.exploded;
	}
	
	@Override
	public void paint(Graphics2D g) {
		update();
		if(!this.exploded) {
			eggSprite.changeDimensions(this);
			g.drawImage(eggSprite.getImg(), (int)this.getX(), (int)this.getY(), (int)this.getWidth(), (int)this.getHeight(), null);
		}
	}
	
}
