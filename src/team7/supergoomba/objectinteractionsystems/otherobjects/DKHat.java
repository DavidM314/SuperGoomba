package team7.supergoomba.objectinteractionsystems.otherobjects;

import java.awt.Graphics2D;
import java.awt.Image;
import java.io.File;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import team7.supergoomba.ObjectInteractionSystem;
import team7.supergoomba.objectinteractionsystems.EnumDirection;
import team7.supergoomba.objectinteractionsystems.GameCharacter;
import team7.supergoomba.objectinteractionsystems.InGameObject;

public class DKHat extends InGameObject implements Projectile {

	private Image[] imgs;
	private int state;
	private ArrayList<GameCharacter> immune;
	private double acceleration;
	
	public DKHat(GameCharacter c, ObjectInteractionSystem oiSystem) {
		super(c.getDirection()==EnumDirection.RIGHT?c.getX() + c.getWidth() + 10:(c.getX() - 60), c.getY() + c.getWidth()/2 - 25, 50, 50, c.getDirection()==EnumDirection.RIGHT?60:-60, 0, false, oiSystem);
		try {
			imgs = new Image[8];
			for(int i = 1; i<=8; i++) {
				imgs[i-1] = ImageIO.read(new File("./resources/sprites/others/diddy_kong_hat/hat" + i + ".png"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
		state = 0;
		immune = new ArrayList<GameCharacter>();
		immune.add(c);
		this.acceleration = c.getDirection()==EnumDirection.RIGHT?-2:2;
	}
	
	public void onCollision(GameCharacter c) {
		if(immune.contains(c)) {
			return;
		}
		immune.add(c);
		c.reduceHP(6, this);
	}
	
	public boolean shouldRemove() {
		return ObjectInteractionSystem.haveCollided(immune.get(0), this) || this.getX()<0 || this.getX()>this.getObjectInteractionSystem().getRenderer().getWidth();  
	}
	
	public void update() {
		this.setX(this.getX() + this.getMotionX());
		this.changeMotionX(acceleration);
	}
	
	public void paint(Graphics2D g) {
		g.drawImage(imgs[state], (int)this.getX(), (int)this.getY(), (int)this.getWidth(), (int)this.getHeight(), null);
		state += 1;
		state %= 8;
		update();
	}

}

