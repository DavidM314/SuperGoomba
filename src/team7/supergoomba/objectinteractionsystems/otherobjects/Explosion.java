package team7.supergoomba.objectinteractionsystems.otherobjects;

import java.awt.Graphics2D;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import team7.supergoomba.ObjectInteractionSystem;
import team7.supergoomba.objectinteractionsystems.GameCharacter;
import team7.supergoomba.objectinteractionsystems.InGameObject;

public class Explosion extends InGameObject implements Projectile {

	private long timeCreated;
	private long totalTime = 1500;
	private ArrayList<GameCharacter> immune;
	private int damage;
	
	public Explosion(double x, double y, ObjectInteractionSystem oiSystem, GameCharacter immune, int damage) {
		super(x - 100, y - 100, 200, 200, 0, 0, false, oiSystem);
		this.timeCreated = System.currentTimeMillis();
		this.immune = new ArrayList<GameCharacter>();
		this.immune.add(immune);
		this.damage = damage;
	}

	@Override
	public void paint(Graphics2D g) {
		long time = System.currentTimeMillis() - timeCreated;
		if(time<=totalTime) {
			for(int i = 1; i<=5; i++) {
				if(time<=totalTime*i/5) {
					Image img;
					try {
						img = ImageIO.read(new File("./resources/sprites/others/explosion/" + i + ".png"));
						g.drawImage(img, (int)this.getX(), (int)this.getY(), (int)this.getWidth(), (int)this.getHeight(), null);
					} catch (IOException e) {
						e.printStackTrace();
					}
					break;
				}
			}
		}
	}
	
	public void onCollision(GameCharacter c) {
		if(immune.contains(c)) {
			return;
		}
		c.setMotionY(-c.getMotionY());
		c.setMotionX(-c.getMotionX());
		c.reduceHP(damage,this);
		immune.add(c);
	}
	
	public boolean shouldRemove() {
		return System.currentTimeMillis()-timeCreated>totalTime;
	}
	
}
