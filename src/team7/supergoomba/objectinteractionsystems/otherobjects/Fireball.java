package team7.supergoomba.objectinteractionsystems.otherobjects;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.io.File;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import team7.supergoomba.ObjectInteractionSystem;
import team7.supergoomba.objectinteractionsystems.EnumDirection;
import team7.supergoomba.objectinteractionsystems.GameCharacter;
import team7.supergoomba.objectinteractionsystems.InGameObject;

public class Fireball extends InGameObject implements Projectile {

	private Image[] imgs;
	private int state;
	private long creationTime;
	private ArrayList<GameCharacter> immune;
	private long maxTime;
	private int dmg;
	
	public Fireball(GameCharacter c, ObjectInteractionSystem oiSystem, double motionX, double motionY, long time) {
		super(c.getDirection()==EnumDirection.RIGHT?c.getX() + c.getWidth():(c.getX()-25), c.getY() + c.getWidth()/2 - 25, 50, 50, motionX, motionY, false, oiSystem);
		try {
			imgs = new Image[4];
			for(int i = 1; i<=4; i++) {
				imgs[i-1] = ImageIO.read(new File("./resources/sprites/others/fireball/red_fireball" + i + ".png"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
		state = 0;
		this.creationTime = System.currentTimeMillis();
		immune = new ArrayList<GameCharacter>();
		immune.add(c);
		this.maxTime = time;
		this.dmg = 2;
	}
	
	public Fireball(GameCharacter c, ObjectInteractionSystem oiSystem, Color color) {
		super(c.getDirection()==EnumDirection.RIGHT?c.getX() + c.getWidth():(c.getX() - 50), c.getY() + c.getWidth()/2 - 25, 50, 50, c.getDirection()==EnumDirection.RIGHT?c.getMaxSpeed()*1.5:-c.getMaxSpeed()*1.5, 0, false, oiSystem);
		try {
			imgs = new Image[4];
			String colorStr = "red";
			if(color==Color.GREEN) {
				colorStr = "green";
			}
			for(int i = 1; i<=4; i++) {
				imgs[i-1] = ImageIO.read(new File("./resources/sprites/others/fireball/" + colorStr + "_fireball" + i + ".png"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
		state = 0;
		this.creationTime = System.currentTimeMillis();
		immune = new ArrayList<GameCharacter>();
		immune.add(c);
	}
	
	public void onCollision(GameCharacter c) {
		if(immune.contains(c)) {
			return;
		}
		immune.add(c);
		if(this.dmg!=0) {
			c.reduceHP(dmg, this);
		} else {
			c.reduceHP(6, this);
		}
	}
	
	public boolean shouldRemove() {
		if(maxTime!=0) {
			return System.currentTimeMillis()-creationTime>this.maxTime;
		}
		return System.currentTimeMillis()-creationTime>5000;
	}
	
	public void update() {
		this.setX(this.getX() + this.getMotionX());
		this.setY(this.getY() + this.getMotionY());
	}
	
	public void paint(Graphics2D g) {
		g.drawImage(imgs[state], (int)this.getX(), (int)this.getY(), (int)this.getWidth(), (int)this.getHeight(), null);
		state += 1;
		state %= 4;
		update();
	}

}
