package team7.supergoomba.objectinteractionsystems;

public enum KeyControlEnum {
	WASD_KEYS,
	ARROW_KEYS,
	NONE
}
