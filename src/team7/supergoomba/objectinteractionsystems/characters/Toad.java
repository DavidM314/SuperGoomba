package team7.supergoomba.objectinteractionsystems.characters;

import java.io.File;
import java.util.Stack;

import team7.supergoomba.ObjectInteractionSystem;
import team7.supergoomba.objectinteractionsystems.CharacterState;
import team7.supergoomba.objectinteractionsystems.GameCharacter;
import team7.supergoomba.objectinteractionsystems.InGameObject;
import team7.supergoomba.objectinteractionsystems.KeyControlEnum;

public class Toad extends GameCharacter {

	public Toad(double x, double y, KeyControlEnum controls, ObjectInteractionSystem oiSystem, boolean isBot) {
		super(x, y, 75, 75, oiSystem, "Toad", 75, 2, 2, new File("./resources/sprites/characters/toad"), controls, isBot, 1500, 20000);
	}

	@Deprecated
	public Toad(Toad t) {
		super(t);
	}
	
	@Override
	public GameCharacter clone() {
		return new Toad(this);
	}
	
	@Override
	public void reduceHP(int amount, InGameObject src) {
		if(this.getStateStack().peek()==CharacterState.S_SPECIAL_ATTACKING && src instanceof GameCharacter) {
			((GameCharacter)src).reduceHP(amount*5, this);
		} else {
			super.reduceHP(amount, src);
		}
	}
	
	@Override
	public void onCollision(GameCharacter g) {
		Stack<CharacterState> stack = this.getStateStack();
		if(stack.peek()==CharacterState.ATTACKING) {
			g.reduceHP(this.getPower(), this);
		} else if (stack.peek()==CharacterState.SPECIAL_ATTACKING) {
			g.reduceHP(this.getPower()*2, this);
		}
	}
	
	@Override
	public void specialAttack() {
		if(System.currentTimeMillis()-this.lastSpecialAttack<=this.specialAttackDelay || this.getStateStack().peek()==CharacterState.ATTACKING || this.getStateStack().peek()==CharacterState.S_SPECIAL_ATTACKING) {
			return;
		}
		this.lastSpecialAttack = System.currentTimeMillis();
		this.getStateStack().push(CharacterState.SPECIAL_ATTACKING);
		new Thread() {
			public void run() {
				try {
					Thread.sleep(1500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if(Toad.this.getStateStack().peek()==CharacterState.SPECIAL_ATTACKING) {
					Toad.this.getStateStack().pop();
				}
			}
		}.start();	
	}

	@Override
	public void superSpecialAttack() {
		if(System.currentTimeMillis()-this.lastSuperSpecialAttack<=this.superSpecialAttackDelay || this.getStateStack().peek()==CharacterState.ATTACKING || this.getStateStack().peek()==CharacterState.SPECIAL_ATTACKING) {
			//return;
		}
		this.lastSuperSpecialAttack = System.currentTimeMillis();
		this.getStateStack().push(CharacterState.S_SPECIAL_ATTACKING);
		new Thread() {
			public void run() {
				while(System.currentTimeMillis()-Toad.this.lastSuperSpecialAttack<=1000) {
					Toad.this.setMotionX(0);
					Toad.this.setMotionY(0);
				}
				if(Toad.this.getStateStack().peek()==CharacterState.S_SPECIAL_ATTACKING) {
					Toad.this.getStateStack().pop();
				}
			}
		}.start();	
	}

}
