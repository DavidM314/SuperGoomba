package team7.supergoomba.objectinteractionsystems.characters;

import java.io.File;
import java.util.Stack;

import team7.supergoomba.AudioPlayer;
import team7.supergoomba.ObjectInteractionSystem;
import team7.supergoomba.objectinteractionsystems.CharacterState;
import team7.supergoomba.objectinteractionsystems.EnumDirection;
import team7.supergoomba.objectinteractionsystems.GameCharacter;
import team7.supergoomba.objectinteractionsystems.KeyControlEnum;
import team7.supergoomba.objectinteractionsystems.otherobjects.Explosion;

public class Daisy extends GameCharacter {
	private AudioPlayer audioPlayer;
	
	public Daisy(double x, double y, KeyControlEnum controls, ObjectInteractionSystem oiSystem, boolean isBot) {
		super(x, y, 75, 75, oiSystem, "Daisy", 100, 4, 1, new File("./resources/sprites/characters/daisy"), controls, isBot, 1500, 20000);
		audioPlayer = new AudioPlayer();
	}
	
	@Deprecated
	public Daisy(Daisy d) {
		super(d);
	}

	@Override
	public GameCharacter clone() {
		return new Daisy(this);
	}
	
	@Override
	public void onCollision(GameCharacter g) {
		Stack<CharacterState> stack = this.getStateStack();
		if(stack.peek()==CharacterState.ATTACKING) {
			g.reduceHP(this.getPower(), this);
			stack.pop();
		} else if (stack.peek()==CharacterState.SPECIAL_ATTACKING) {
			g.reduceHP(this.getPower(), this);
		}
	}
	
	@Override
	public void specialAttack() {
		if(System.currentTimeMillis()-this.lastSpecialAttack<=this.specialAttackDelay || this.getStateStack().peek()==CharacterState.ATTACKING || this.getStateStack().peek()==CharacterState.S_SPECIAL_ATTACKING) {
			return;
		}
		this.lastSpecialAttack = System.currentTimeMillis();
		this.getStateStack().push(CharacterState.SPECIAL_ATTACKING);
		new Thread() {
			public void run() {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if(Daisy.this.getStateStack().peek()==CharacterState.SPECIAL_ATTACKING) {
					Daisy.this.getStateStack().pop();
				}
			}
		}.start();	
	}

	@Override
	public void superSpecialAttack() {
		if(System.currentTimeMillis()-this.lastSuperSpecialAttack<=this.superSpecialAttackDelay || this.getStateStack().peek()==CharacterState.ATTACKING || this.getStateStack().peek()==CharacterState.SPECIAL_ATTACKING) {
			return;
		}
		this.lastSuperSpecialAttack = System.currentTimeMillis();
		this.getStateStack().push(CharacterState.S_SPECIAL_ATTACKING);
		
		new Thread() {
			@SuppressWarnings("static-access")
			public void run() {
				while(System.currentTimeMillis()-Daisy.this.lastSuperSpecialAttack<500) {
					Daisy.this.setMotionX(0);
					Daisy.this.setMotionY(0);
				}
				if(Daisy.this.getStateStack().peek()==CharacterState.S_SPECIAL_ATTACKING) {
					Toad t = new Toad(Daisy.this.getDirection()==EnumDirection.RIGHT?(Daisy.this.getX() + Daisy.this.getWidth() + 5):(Daisy.this.getX() - 80), Daisy.this.getY(), null, Daisy.this.getObjectInteractionSystem(), false);
					t.setDirection(Daisy.this.getDirection());
					t.setMotionX(Daisy.this.getDirection()==EnumDirection.RIGHT?50:-50);
					t.getStateStack().push(CharacterState.S_SPECIAL_ATTACKING);
					Daisy.this.getObjectInteractionSystem().add(t);
					
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					Daisy.this.getStateStack().pop();
					try {
						Thread.sleep(750);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					audioPlayer.getSoundEffect().EXPLOSION.play();
					Daisy.this.getObjectInteractionSystem().add(new Explosion(t.getX() + t.getWidth()/2, t.getY() + t.getHeight()/2, Daisy.this.getObjectInteractionSystem(), Daisy.this, 5));
					Daisy.this.getObjectInteractionSystem().remove(t);
				}
			}
		}.start();	
	}

}
