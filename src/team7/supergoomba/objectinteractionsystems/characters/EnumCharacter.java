package team7.supergoomba.objectinteractionsystems.characters;

public enum EnumCharacter {
	BOO,
	BOWSER,
	BOWSER_JR,
	DAISY,
	DIDDY_KONG,
	DONKEY_KONG,
	GOOMBA,
	HAMMER_BRO,
	KOOPA_TROOPA,
	LUIGI,
	MARIO,
	PEACH,
	SHY_GUY,
	SPINY,
	TOAD,
	YOSHI
}
