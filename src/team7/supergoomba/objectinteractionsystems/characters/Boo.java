package team7.supergoomba.objectinteractionsystems.characters;

import java.io.File;
import java.util.Stack;

import team7.supergoomba.AudioPlayer;
import team7.supergoomba.ObjectInteractionSystem;
import team7.supergoomba.objectinteractionsystems.CharacterState;
import team7.supergoomba.objectinteractionsystems.EnumDirection;
import team7.supergoomba.objectinteractionsystems.GameCharacter;
import team7.supergoomba.objectinteractionsystems.KeyControlEnum;
import team7.supergoomba.objectinteractionsystems.otherobjects.Explosion;

public class Boo extends GameCharacter {
	private AudioPlayer audioPlayer;
	
	public Boo(double x, double y, KeyControlEnum controls, ObjectInteractionSystem oiSystem, boolean isBot) {
		super(x, y, 75, 75, oiSystem, "Boo", 50, 3, 5, new File("./resources/sprites/characters/boo"), controls, isBot, 1500, 20000);
		audioPlayer = new AudioPlayer();
	}

	@Deprecated
	public Boo(Boo b) {
		super(b);
	}
	
	@Override
	public GameCharacter clone() {
		return new Boo(this);
	}
	
	@Override
	public void onCollision(GameCharacter g) {
		Stack<CharacterState> stack = this.getStateStack();
		if(stack.peek()==CharacterState.ATTACKING) {
			g.reduceHP(this.getPower(),this);
			stack.pop();
		} else if (stack.peek()==CharacterState.S_SPECIAL_ATTACKING) {
			g.reduceHP(this.getPower()*3,this);
			int newHealth = Math.min(this.getHealth()+this.getPower(), this.getMaxHealth());
			this.reduceHP(this.getHealth()-newHealth,this);
			stack.pop();
		}
	}
	
	@Override
	public void specialAttack() {
		if(System.currentTimeMillis()-lastSpecialAttack>this.specialAttackDelay && !(this.getStateStack().peek()==CharacterState.ATTACKING || this.getStateStack().peek()==CharacterState.S_SPECIAL_ATTACKING)) {
			lastSpecialAttack = System.currentTimeMillis();
		} else {
			return;
		}
		this.getStateStack().push(CharacterState.SPECIAL_ATTACKING);
		new Thread() {
			@SuppressWarnings("static-access")
			public void run() {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if(Boo.this.getStateStack().peek()==CharacterState.SPECIAL_ATTACKING) {
					Boo.this.getStateStack().pop();
				}
				if(Boo.this.getDirection()==EnumDirection.RIGHT) {
					Boo.this.setX(Math.min(Boo.this.getX() + 300, Boo.this.getObjectInteractionSystem().getRenderer().getWidth() - Boo.this.getWidth()));
				} else {
					Boo.this.setX(Math.max(Boo.this.getX() - 300, 0));
				}
				audioPlayer.getSoundEffect().EXPLOSION.play();
				Boo.this.getObjectInteractionSystem().add(new Explosion(Boo.this.getX() + Boo.this.getWidth()/2, Boo.this.getY() + Boo.this.getHeight()/2, Boo.this.getObjectInteractionSystem(), Boo.this, 4));
			}
		}.start();
	}

	@Override
	public void superSpecialAttack() {
		if(System.currentTimeMillis()-lastSuperSpecialAttack>this.superSpecialAttackDelay && !(this.getStateStack().peek()==CharacterState.ATTACKING || this.getStateStack().peek()==CharacterState.SPECIAL_ATTACKING)) {
			lastSuperSpecialAttack = System.currentTimeMillis();
		} else {
			return;
		}
		this.getStateStack().push(CharacterState.S_SPECIAL_ATTACKING);
		new Thread() {
			public void run() {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if(Boo.this.getStateStack().peek()==CharacterState.S_SPECIAL_ATTACKING) {
					Boo.this.getStateStack().pop();
				}
			}
		}.start();
	}

}
