package team7.supergoomba.objectinteractionsystems.characters;

import java.io.File;
import java.util.Stack;

import team7.supergoomba.ObjectInteractionSystem;
import team7.supergoomba.objectinteractionsystems.CharacterState;
import team7.supergoomba.objectinteractionsystems.GameCharacter;
import team7.supergoomba.objectinteractionsystems.KeyControlEnum;
import team7.supergoomba.objectinteractionsystems.otherobjects.Barrel;

public class DonkeyKong extends GameCharacter {

	public DonkeyKong(double x, double y, KeyControlEnum controls, ObjectInteractionSystem oiSystem, boolean isBot) {
		super(x, y, 75, 75, oiSystem, "Donkey Kong", 125, 3, 2, new File("./resources/sprites/characters/donkey_kong"), controls, isBot, 1500, 20000);
	}

	@Deprecated
	public DonkeyKong(DonkeyKong d) {
		super(d);
	}
	
	@Override
	public GameCharacter clone() {
		return new DonkeyKong(this);
	}
	
	@Override
	public void onCollision(GameCharacter g) {
		Stack<CharacterState> stack = this.getStateStack();
		if(stack.peek()==CharacterState.ATTACKING) {
			g.reduceHP(this.getPower(), this);
			stack.pop();
		} else if (stack.peek()==CharacterState.SPECIAL_ATTACKING) {
			g.reduceHP(this.getPower()*2, this);
			stack.pop();
		} else if (stack.peek()==CharacterState.S_SPECIAL_ATTACKING) {
			
		}
	}
	
	@Override
	public void specialAttack() {
		if(System.currentTimeMillis()-this.lastSpecialAttack<=this.specialAttackDelay || this.getStateStack().peek()==CharacterState.ATTACKING || this.getStateStack().peek()==CharacterState.S_SPECIAL_ATTACKING) {
			return;
		}
		this.lastSpecialAttack = System.currentTimeMillis();
		this.getStateStack().push(CharacterState.SPECIAL_ATTACKING);
		this.setMotionY(50);
		this.setY(this.getY() - 10);
		this.setOnGround(false);
		new Thread() {
			public void run() {
				try {
					Thread.sleep(1200);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if(DonkeyKong.this.getStateStack().peek()==CharacterState.SPECIAL_ATTACKING) {
					DonkeyKong.this.getStateStack().pop();
				}
			}
		}.start();	
	}

	@Override
	public void superSpecialAttack() {
		if(System.currentTimeMillis()-this.lastSuperSpecialAttack<=this.superSpecialAttackDelay || this.getStateStack().peek()==CharacterState.ATTACKING || this.getStateStack().peek()==CharacterState.SPECIAL_ATTACKING) {
			return;
		}
		this.lastSuperSpecialAttack = System.currentTimeMillis();
		this.getStateStack().push(CharacterState.S_SPECIAL_ATTACKING);
		
		new Thread() {
			public void run() {
				while(System.currentTimeMillis()-DonkeyKong.this.lastSuperSpecialAttack<500) {
					DonkeyKong.this.setMotionX(0);
					DonkeyKong.this.setMotionY(0);
				}
				if(DonkeyKong.this.getStateStack().peek()==CharacterState.S_SPECIAL_ATTACKING) {
					Barrel b = new Barrel(DonkeyKong.this.getObjectInteractionSystem(), DonkeyKong.this);
					DonkeyKong.this.getObjectInteractionSystem().add(b);
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					DonkeyKong.this.getStateStack().pop();
				}
			}
		}.start();	
	}

}
