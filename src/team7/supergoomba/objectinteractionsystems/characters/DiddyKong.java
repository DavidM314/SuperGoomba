package team7.supergoomba.objectinteractionsystems.characters;

import java.io.File;

import team7.supergoomba.ObjectInteractionSystem;
import team7.supergoomba.objectinteractionsystems.CharacterState;
import team7.supergoomba.objectinteractionsystems.GameCharacter;
import team7.supergoomba.objectinteractionsystems.KeyControlEnum;
import team7.supergoomba.objectinteractionsystems.otherobjects.Barrel;
import team7.supergoomba.objectinteractionsystems.otherobjects.DKHat;

public class DiddyKong extends GameCharacter {
	
	public DiddyKong(double x, double y, KeyControlEnum controls, ObjectInteractionSystem oiSystem, boolean isBot) {
		super(x, y, 75, 75, oiSystem, "Diddy Kong", 50, 3, 5, new File("./resources/sprites/characters/diddy_kong"), controls, isBot, 1500, 20000);
	}

	@Deprecated
	public DiddyKong(DiddyKong d) {
		super(d);
	}
	
	@Override
	public GameCharacter clone() {
		return new DiddyKong(this);
	}
	
	@Override
	public void onCollision(GameCharacter g) {
		if(this.getStateStack().peek()==CharacterState.ATTACKING) {
			g.reduceHP(this.getPower(), this);
			this.getStateStack().pop();
		}
	}
	
	@Override
	public void specialAttack() {
		if(System.currentTimeMillis()-this.lastSpecialAttack<=this.specialAttackDelay || this.getStateStack().peek()==CharacterState.ATTACKING || this.getStateStack().peek()==CharacterState.S_SPECIAL_ATTACKING) {
			return;
		}
		this.lastSpecialAttack = System.currentTimeMillis();
		this.getStateStack().push(CharacterState.SPECIAL_ATTACKING);
		
		new Thread() {
			public void run() {
				while(System.currentTimeMillis()-DiddyKong.this.lastSpecialAttack<500) {
					DiddyKong.this.setMotionX(0);
					DiddyKong.this.setMotionY(0);
				}
				if(DiddyKong.this.getStateStack().peek()==CharacterState.SPECIAL_ATTACKING) {
					Barrel b = new Barrel(DiddyKong.this.getObjectInteractionSystem(), DiddyKong.this,0.5);
					DiddyKong.this.getObjectInteractionSystem().add(b);
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					DiddyKong.this.getStateStack().pop();
				}
			}
		}.start();	
	}

	@Override
	public void superSpecialAttack() {
		if(System.currentTimeMillis()-this.lastSuperSpecialAttack<=this.superSpecialAttackDelay || this.getStateStack().peek()==CharacterState.ATTACKING || this.getStateStack().peek()==CharacterState.SPECIAL_ATTACKING) {
			return;
		}
		this.lastSuperSpecialAttack = System.currentTimeMillis();
		this.getStateStack().push(CharacterState.S_SPECIAL_ATTACKING);
		
		new Thread() {
			public void run() {
				while(System.currentTimeMillis()-DiddyKong.this.lastSuperSpecialAttack<500) {
					DiddyKong.this.setMotionX(0);
					DiddyKong.this.setMotionY(0);
				}
				if(DiddyKong.this.getStateStack().peek()==CharacterState.S_SPECIAL_ATTACKING) {
					DKHat hat = new DKHat(DiddyKong.this, DiddyKong.this.getObjectInteractionSystem());
					DiddyKong.this.getObjectInteractionSystem().add(hat);
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					DiddyKong.this.getStateStack().pop();
				}
			}
		}.start();	
	}

}
