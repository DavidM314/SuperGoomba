package team7.supergoomba.objectinteractionsystems.characters;

import java.awt.event.KeyEvent;
import java.io.File;
import java.util.ArrayList;

import team7.supergoomba.AudioPlayer;
import team7.supergoomba.ObjectInteractionSystem;
import team7.supergoomba.objectinteractionsystems.CharacterState;
import team7.supergoomba.objectinteractionsystems.GameCharacter;
import team7.supergoomba.objectinteractionsystems.KeyControlEnum;
import team7.supergoomba.objectinteractionsystems.otherobjects.Laser;

public class ShyGuy extends GameCharacter {

	private boolean shyStack;
	private AudioPlayer audioPlayer;

	public ShyGuy(double x, double y, KeyControlEnum controls, ObjectInteractionSystem oiSystem, boolean isBot, boolean shyStack) {
		super(x, y, 75, 75, oiSystem, "Shy Guy", 75, 1, 4, new File("./resources/sprites/characters/shy_guy"), controls, isBot, 1500, 20000);
		this.shyStack = shyStack;
		this.audioPlayer = new AudioPlayer();
	}

	public boolean isShyStack() {
		return System.currentTimeMillis()-this.lastSuperSpecialAttack<5000 || this.shyStack;
	}
	
	@Deprecated
	public ShyGuy(ShyGuy s) {
		super(s);
	}
	
	@Override
	public GameCharacter clone() {
		return new ShyGuy(this);
	}
	
	@Override
	public void onCollision(GameCharacter g) {
		if(this.isShyStack()) {
			g.reduceHP(this.getPower()*2, this);
		} else {
			if(this.getStateStack().peek()==CharacterState.ATTACKING) {
				g.reduceHP(this.getPower(), this);
			} 
		}
	}
	
	@SuppressWarnings("static-access")
	@Override
	public void specialAttack() {
		long time = System.currentTimeMillis();
		if(!(time-this.lastSpecialAttack>this.specialAttackDelay && this.getStateStack().peek()!=CharacterState.ATTACKING && this.getStateStack().peek()!=CharacterState.S_SPECIAL_ATTACKING)) {
			return;
		}		
		this.lastSpecialAttack = System.currentTimeMillis();
		audioPlayer.getSoundEffect().LASER.play();
		Laser laser = new Laser(this, this.getObjectInteractionSystem());
		this.getObjectInteractionSystem().add(laser);
	}

	@SuppressWarnings("static-access")
	@Override
	public void superSpecialAttack() {
		long time = System.currentTimeMillis();
		if(!(time-this.lastSuperSpecialAttack>this.superSpecialAttackDelay && time-this.lastSpecialAttack>1000 && this.getStateStack().peek()!=CharacterState.ATTACKING)) {
			return;
		}	
		this.lastSuperSpecialAttack = System.currentTimeMillis();
		audioPlayer.getSoundEffect().POWERUP.play();
		ArrayList<ShyGuy> shyStack = new ArrayList<ShyGuy>();
		for(int i = 0; i<5; i++) {
			ShyGuy sg = new ShyGuy(this.getX(), this.getY() - (i+1)*this.getHeight(), this.getControls(), this.getObjectInteractionSystem(), false, true) {
				@Override
				public void keyPressed(KeyEvent e) {}
			};
			sg.setOnGround(false);
			sg.setGravityAffected(false);
			shyStack.add(sg);
			this.getObjectInteractionSystem().add(sg);
		}
		new Thread() {
			public void run() {
				while(System.currentTimeMillis()-lastSuperSpecialAttack<5000) {
					for(int i = 0; i<5; i++) {
						ShyGuy sg = shyStack.get(i);
						sg.setX(ShyGuy.this.getX());
						sg.setY(ShyGuy.this.getY() - (i+1)*ShyGuy.this.getHeight());
					}
				}
				for(ShyGuy sg : shyStack) {
					ShyGuy.this.getObjectInteractionSystem().remove(sg);
				}
			}
		}.start();
	}

}
