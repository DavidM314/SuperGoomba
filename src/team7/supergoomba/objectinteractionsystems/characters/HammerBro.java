package team7.supergoomba.objectinteractionsystems.characters;

import java.io.File;

import team7.supergoomba.ObjectInteractionSystem;
import team7.supergoomba.objectinteractionsystems.CharacterState;
import team7.supergoomba.objectinteractionsystems.GameCharacter;
import team7.supergoomba.objectinteractionsystems.KeyControlEnum;
import team7.supergoomba.objectinteractionsystems.otherobjects.Hammer;

public class HammerBro extends GameCharacter {

	public HammerBro(double x, double y, KeyControlEnum controls, ObjectInteractionSystem oiSystem, boolean isBot) {
		super(x, y, 75, 75, oiSystem, "Hammer Bro", 75, 3, 2, new File("./resources/sprites/characters/hammer_bro"), controls, isBot, 1500, 20000);
	}

	@Deprecated
	public HammerBro(HammerBro h) {
		super(h);
	}
	
	@Override
	public void onCollision(GameCharacter g) {
		if(this.getStateStack().peek()==CharacterState.ATTACKING) {
			g.reduceHP(this.getPower(), this);
			this.getStateStack().pop();
		}
	}
	
	@Override
	public GameCharacter clone() {
		return new HammerBro(this);
	}
	
	@Override
	public void specialAttack() {
		if(System.currentTimeMillis()-this.lastSpecialAttack<=this.specialAttackDelay || this.getStateStack().peek()==CharacterState.ATTACKING || this.getStateStack().peek()==CharacterState.S_SPECIAL_ATTACKING) {
			return;
		}
		this.lastSpecialAttack = System.currentTimeMillis();
		this.getStateStack().push(CharacterState.SPECIAL_ATTACKING);
		
		new Thread() {
			public void run() {
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if(HammerBro.this.getStateStack().peek()==CharacterState.SPECIAL_ATTACKING) {
					Hammer hammer = new Hammer(HammerBro.this.getObjectInteractionSystem(), HammerBro.this,false);
					HammerBro.this.getObjectInteractionSystem().add(hammer);
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					HammerBro.this.getStateStack().pop();
				}
			}
		}.start();	
	}


	@Override
	public void superSpecialAttack() {
		if(System.currentTimeMillis()-this.lastSuperSpecialAttack<=this.superSpecialAttackDelay || this.getStateStack().peek()==CharacterState.ATTACKING || this.getStateStack().peek()==CharacterState.SPECIAL_ATTACKING) {
			return;
		}
		this.lastSuperSpecialAttack = System.currentTimeMillis();
		this.getStateStack().push(CharacterState.S_SPECIAL_ATTACKING);
		
		new Thread() {
			public void run() {
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if(HammerBro.this.getStateStack().peek()==CharacterState.S_SPECIAL_ATTACKING) {
					Hammer hammer = new Hammer(HammerBro.this.getObjectInteractionSystem(), HammerBro.this,true);
					HammerBro.this.getObjectInteractionSystem().add(hammer);
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					HammerBro.this.getStateStack().pop();
				}
			}
		}.start();	
	}

}
