package team7.supergoomba.objectinteractionsystems.characters;

import java.io.File;
import java.util.Stack;

import team7.supergoomba.ObjectInteractionSystem;
import team7.supergoomba.objectinteractionsystems.CharacterState;
import team7.supergoomba.objectinteractionsystems.GameCharacter;
import team7.supergoomba.objectinteractionsystems.InGameObject;
import team7.supergoomba.objectinteractionsystems.KeyControlEnum;

public class Peach extends GameCharacter {

	public Peach(double x, double y, KeyControlEnum controls, ObjectInteractionSystem oiSystem, boolean isBot) {
		super(x, y, 75, 75, oiSystem, "Peach", 100, 2, 3, new File("./resources/sprites/characters/peach"), controls, isBot, 1500, 20000);
	}

	@Deprecated
	public Peach(Peach p) {
		super(p);
	}
	
	@Override
	public GameCharacter clone() {
		return new Peach(this);
	}
	
	@Override
	public void onCollision(GameCharacter g) {
		Stack<CharacterState> stack = this.getStateStack();
		if(stack.peek()==CharacterState.ATTACKING) {
			g.reduceHP(this.getPower(), this);
		} else if (stack.peek()==CharacterState.SPECIAL_ATTACKING) {
			g.reduceHP(this.getPower()*2, this);
		}
	}
	
	@Override
	public void reduceHP(int amount, InGameObject o) {
		if(!o.equals(this) && this.getStateStack().peek()==CharacterState.SPECIAL_ATTACKING) {
			return;
		}
		super.reduceHP(amount, o);
	}
	
	@Override
	public void specialAttack() {
		if(System.currentTimeMillis()-this.lastSpecialAttack<=this.specialAttackDelay || this.getStateStack().peek()==CharacterState.ATTACKING || this.getStateStack().peek()==CharacterState.S_SPECIAL_ATTACKING) {
			return;
		}
		this.lastSpecialAttack = System.currentTimeMillis();
		this.getStateStack().push(CharacterState.SPECIAL_ATTACKING);
		new Thread() {
			public void run() {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if(Peach.this.getStateStack().peek()==CharacterState.SPECIAL_ATTACKING) {
					Peach.this.getStateStack().pop();
				}
			}
		}.start();	
	}

	@Override
	public void superSpecialAttack() {
		if(System.currentTimeMillis()-this.lastSuperSpecialAttack<=this.superSpecialAttackDelay || this.getStateStack().peek()==CharacterState.ATTACKING || this.getStateStack().peek()==CharacterState.SPECIAL_ATTACKING) {
			return;
		}
		this.lastSuperSpecialAttack = System.currentTimeMillis();
		this.getStateStack().push(CharacterState.S_SPECIAL_ATTACKING);
		this.reduceHP(-10, this);
		if(this.getPercentHealth()>1.0) {
			this.reduceHP(this.getHealth()-this.getMaxHealth(), this);
		}
		new Thread() {
			public void run() {
				try {
					Thread.sleep(1500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if(Peach.this.getStateStack().peek()==CharacterState.S_SPECIAL_ATTACKING) {
					Peach.this.getStateStack().pop();
				}
			}
		}.start();	
	}

}
