package team7.supergoomba.objectinteractionsystems.characters;

import java.io.File;
import java.util.Stack;

import team7.supergoomba.ObjectInteractionSystem;
import team7.supergoomba.objectinteractionsystems.CharacterState;
import team7.supergoomba.objectinteractionsystems.EnumDirection;
import team7.supergoomba.objectinteractionsystems.GameCharacter;
import team7.supergoomba.objectinteractionsystems.KeyControlEnum;

public class BowserJr extends GameCharacter {

	public BowserJr(double x, double y, KeyControlEnum controls, ObjectInteractionSystem oiSystem, boolean isBot) {
		super(x, y, 75, 75, oiSystem, "Bowser Jr.", 100, 4, 2, new File("./resources/sprites/characters/bowser_jr"), controls, isBot, 1500, 20000);
	}

	@Deprecated
	public BowserJr(BowserJr b) {
		super(b);
	}
	
	@Override
	public GameCharacter clone() {
		return new BowserJr(this);
	}
	
	@Override
	public void onCollision(GameCharacter g) {
		Stack<CharacterState> stack = this.getStateStack();
		if(stack.peek()==CharacterState.ATTACKING) {
			g.reduceHP(this.getPower(), this);
			stack.pop();
		} else if (stack.peek()==CharacterState.SPECIAL_ATTACKING) {
			g.reduceHP(this.getPower()*2, this);
			stack.pop();
		} else if (stack.peek()==CharacterState.S_SPECIAL_ATTACKING) {
			g.reduceHP(this.getPower()*3, this);
		}
	}
	
	@Override
	public void specialAttack() {
		if(System.currentTimeMillis()-this.lastSpecialAttack<=this.specialAttackDelay || this.getStateStack().peek()==CharacterState.ATTACKING || this.getStateStack().peek()==CharacterState.S_SPECIAL_ATTACKING) {
			return;
		}
		this.lastSpecialAttack = System.currentTimeMillis();
		this.getStateStack().push(CharacterState.SPECIAL_ATTACKING);
		new Thread() {
			public void run() {
				try {
					Thread.sleep(2500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if(BowserJr.this.getStateStack().peek()==CharacterState.SPECIAL_ATTACKING) {
					BowserJr.this.getStateStack().pop();
				}
			}
		}.start();
	}

	@Override
	public void superSpecialAttack() {
		if(System.currentTimeMillis()-this.lastSuperSpecialAttack<=this.superSpecialAttackDelay || this.getStateStack().peek()==CharacterState.ATTACKING || this.getStateStack().peek()==CharacterState.SPECIAL_ATTACKING) {
			return;
		}
		this.lastSuperSpecialAttack = System.currentTimeMillis();
		this.getStateStack().push(CharacterState.S_SPECIAL_ATTACKING);
		double oldMaxSpeed = this.getMaxSpeed();
		this.setMaxSpeed(30);
		if(this.getDirection()==EnumDirection.RIGHT) {
			this.setMotionX(30);
		} else {
			this.setMotionX(-30);
		}		
		new Thread() {
			public void run() {
				try {
					Thread.sleep(1500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if(BowserJr.this.getStateStack().peek()==CharacterState.S_SPECIAL_ATTACKING) {
					BowserJr.this.getStateStack().pop();
					BowserJr.this.setMaxSpeed(oldMaxSpeed);
				}
			}
		}.start();
	}

}
