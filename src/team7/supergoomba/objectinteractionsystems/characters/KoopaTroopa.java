package team7.supergoomba.objectinteractionsystems.characters;

import java.io.File;
import java.util.Stack;

import team7.supergoomba.AudioPlayer;
import team7.supergoomba.ObjectInteractionSystem;
import team7.supergoomba.objectinteractionsystems.CharacterState;
import team7.supergoomba.objectinteractionsystems.EnumDirection;
import team7.supergoomba.objectinteractionsystems.GameCharacter;
import team7.supergoomba.objectinteractionsystems.KeyControlEnum;

public class KoopaTroopa extends GameCharacter {

	private double width, height;
	private AudioPlayer audioPlayer;
	
	public KoopaTroopa(double x, double y, KeyControlEnum controls, ObjectInteractionSystem oiSystem, boolean isBot) {
		super(x, y, 75, 75, oiSystem, "KoopaTroopa", 100, 2, 3, new File("./resources/sprites/characters/koopa_troopa"), controls, isBot, 1500, 20000);
		audioPlayer = new AudioPlayer();
	}

	@Deprecated
	public KoopaTroopa(KoopaTroopa k) {
		super(k);
	}
	
	@Override
	public void onCollision(GameCharacter g) {
		Stack<CharacterState> stack = this.getStateStack();
		if(stack.peek()==CharacterState.ATTACKING) {
			g.reduceHP(this.getPower(),this);
			KoopaTroopa.this.setY(this.getY() + this.getHeight() - height);
			this.setHeight(height);
			this.setWidth(width);
			stack.pop();
		} else if (stack.peek()==CharacterState.SPECIAL_ATTACKING) {
			g.reduceHP(this.getPower()*2,this);
			KoopaTroopa.this.setY(this.getY() + this.getHeight() - height);
			KoopaTroopa.this.setWidth(width);
			KoopaTroopa.this.setHeight(height);
			stack.pop();
		} else if (stack.peek()==CharacterState.S_SPECIAL_ATTACKING) {
			g.reduceHP(this.getPower()*2,this);
		}
	}
	
	@Override
	public GameCharacter clone() {
		return new KoopaTroopa(this);
	}
	
	@SuppressWarnings("static-access")
	@Override
	public void attack() {
		if(System.currentTimeMillis()-this.getLastAttack()<this.attackDelay || this.getStateStack().peek() == CharacterState.SPECIAL_ATTACKING || this.getStateStack().peek() == CharacterState.S_SPECIAL_ATTACKING) {
			return;
		}
		audioPlayer.getSoundEffect().SHELL.play();
		this.setLastAttack(System.currentTimeMillis());
		this.getStateStack().push(CharacterState.ATTACKING);
		this.height = this.getHeight();
		this.width = this.getWidth();
		this.setHeight(this.getHeight()/2.0);
		this.setWidth(this.getWidth()/2.0);
		new Thread() {
			public void run() {
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if(KoopaTroopa.this.getStateStack().peek()==CharacterState.ATTACKING) {
					KoopaTroopa.this.setY(KoopaTroopa.this.getY() + KoopaTroopa.this.getHeight() - height);
					KoopaTroopa.this.setWidth(width);
					KoopaTroopa.this.setHeight(height);
					KoopaTroopa.this.getStateStack().pop();
				}
			}
		}.start();
	}
	
	@SuppressWarnings("static-access")
	@Override
	public void specialAttack() {
		if(System.currentTimeMillis()-lastSpecialAttack>this.specialAttackDelay && !(this.getStateStack().peek()==CharacterState.ATTACKING || this.getStateStack().peek()==CharacterState.S_SPECIAL_ATTACKING)) {
			lastSpecialAttack = System.currentTimeMillis();
		} else {
			return;
		}
		audioPlayer.getSoundEffect().SHELL.play();
		this.width = this.getWidth();
		this.height = this.getHeight();
		this.setWidth(this.getWidth()/2.0);
		this.setHeight(this.getHeight()/2.0);
		this.getStateStack().push(CharacterState.SPECIAL_ATTACKING);
		double oldMaxSpeed = this.getMaxSpeed();
		this.setMaxSpeed(this.getMaxSpeed()*3);
		if(this.getDirection()==EnumDirection.RIGHT) {
			this.setMotionX(this.getMaxSpeed());
		} else {
			this.setMotionX(-this.getMaxSpeed());
		}
		new Thread() {
			public void run() {
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if(KoopaTroopa.this.getStateStack().peek()==CharacterState.SPECIAL_ATTACKING) {
					KoopaTroopa.this.setY(KoopaTroopa.this.getY() + KoopaTroopa.this.getHeight() - height);
					KoopaTroopa.this.setWidth(width);
					KoopaTroopa.this.setHeight(height);
					KoopaTroopa.this.getStateStack().pop();
				}
				KoopaTroopa.this.setMaxSpeed(oldMaxSpeed);
			}
		}.start();
	}

	@SuppressWarnings("static-access")
	@Override
	public void superSpecialAttack() {
		if(System.currentTimeMillis()-lastSuperSpecialAttack>this.superSpecialAttackDelay && this.getStateStack().peek()!=CharacterState.ATTACKING && this.getStateStack().peek()!=CharacterState.SPECIAL_ATTACKING) {
			lastSuperSpecialAttack = System.currentTimeMillis();
		} else {
			return;
		}
		audioPlayer.getSoundEffect().POWERUP.play();
		double oldWidth = this.getWidth();
		double oldHeight = this.getHeight();
		double oldMaxSpeed = this.getMaxSpeed();
		this.setWidth(oldWidth*2.0);
		this.setHeight(oldHeight*2.0);
		this.setMaxSpeed(oldMaxSpeed*2.0);
		this.getStateStack().push(CharacterState.S_SPECIAL_ATTACKING);
		this.setGravityAffected(false);
		
		new Thread() {
			public void run() {
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if(KoopaTroopa.this.getStateStack().peek()==CharacterState.S_SPECIAL_ATTACKING) {
					KoopaTroopa.this.getStateStack().pop();
				}
				KoopaTroopa.this.setGravityAffected(true);
				KoopaTroopa.this.setHeight(oldHeight);
				KoopaTroopa.this.setWidth(oldWidth);
				KoopaTroopa.this.setMaxSpeed(oldMaxSpeed);
			}
		}.start();
	}
}
