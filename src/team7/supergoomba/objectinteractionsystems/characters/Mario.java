package team7.supergoomba.objectinteractionsystems.characters;

import java.awt.Color;
import java.io.File;
import java.util.Stack;

import team7.supergoomba.AudioPlayer;
import team7.supergoomba.ObjectInteractionSystem;
import team7.supergoomba.objectinteractionsystems.CharacterState;
import team7.supergoomba.objectinteractionsystems.GameCharacter;
import team7.supergoomba.objectinteractionsystems.KeyControlEnum;
import team7.supergoomba.objectinteractionsystems.otherobjects.Fireball;

public class Mario extends GameCharacter {
	private AudioPlayer audioPlayer;
	
	public Mario(double x, double y, KeyControlEnum controls, ObjectInteractionSystem oiSystem, boolean isBot) {
		super(x, y, 60, 75, oiSystem, "Mario", 100, 4, 3, new File("./resources/sprites/characters/mario"), controls, isBot, 1500, 20000);
		audioPlayer = new AudioPlayer();
	}

	@Deprecated
	public Mario(Mario m) {
		super(m);
	}
	
	@Override
	public GameCharacter clone() {
		return new Mario(this);
	}
	
	@Override
	public void onCollision(GameCharacter g) {
		Stack<CharacterState> stack = this.getStateStack();
		if(stack.peek()==CharacterState.ATTACKING) {
			g.reduceHP(this.getPower(), this);
			stack.pop();
		} else if (stack.peek()==CharacterState.S_SPECIAL_ATTACKING) {
			g.reduceHP(this.getPower()*5, this);
			stack.pop();
		}
	}
	
	@SuppressWarnings("static-access")
	@Override
	public void specialAttack() {
		if(System.currentTimeMillis()-lastSpecialAttack<=this.specialAttackDelay || (this.getStateStack().peek()==CharacterState.ATTACKING || this.getStateStack().peek()==CharacterState.S_SPECIAL_ATTACKING)) {
			return;
		}
		this.lastSpecialAttack = System.currentTimeMillis();
		audioPlayer.getSoundEffect().FIREBALL.play();
		this.getStateStack().push(CharacterState.SPECIAL_ATTACKING);
		Fireball f = new Fireball(this,this.getObjectInteractionSystem(),Color.RED);
		this.getObjectInteractionSystem().add(f);
		new Thread() {
			public void run() {
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if(Mario.this.getStateStack().peek()==CharacterState.SPECIAL_ATTACKING) {
					Mario.this.getStateStack().pop();
				}
			}
		}.start();
	}

	@SuppressWarnings("static-access")
	@Override
	public void superSpecialAttack() {
		if(System.currentTimeMillis()-lastSuperSpecialAttack<=this.superSpecialAttackDelay || (this.getStateStack().peek()==CharacterState.ATTACKING || this.getStateStack().peek()==CharacterState.SPECIAL_ATTACKING)) {
			return;
		}
		this.lastSuperSpecialAttack = System.currentTimeMillis();
		audioPlayer.getSoundEffect().WHACK.play();
		this.getStateStack().push(CharacterState.S_SPECIAL_ATTACKING);
		new Thread() {
			public void run() {
				try {
					Thread.sleep(750);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if(Mario.this.getStateStack().peek()==CharacterState.S_SPECIAL_ATTACKING) {
					Mario.this.getStateStack().pop();
				}
			}
		}.start();
	}

}
