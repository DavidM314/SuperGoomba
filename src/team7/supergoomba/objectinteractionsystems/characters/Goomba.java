package team7.supergoomba.objectinteractionsystems.characters;

import java.io.File;
import java.util.Stack;

import team7.supergoomba.AudioPlayer;
import team7.supergoomba.ObjectInteractionSystem;
import team7.supergoomba.objectinteractionsystems.CharacterState;
import team7.supergoomba.objectinteractionsystems.EnumDirection;
import team7.supergoomba.objectinteractionsystems.GameCharacter;
import team7.supergoomba.objectinteractionsystems.KeyControlEnum;

public class Goomba extends GameCharacter {
	
	private AudioPlayer audioPlayer;
	
	public Goomba(double x, double y, KeyControlEnum controls, ObjectInteractionSystem oiSystem, boolean isBot) {
		super(x, y, 75, 75, oiSystem, "Goomba", 75, 2, 2, new File("./resources/sprites/characters/goomba"), controls, isBot, 1500, 20000);
		audioPlayer = new AudioPlayer();
	}

	@Deprecated
	public Goomba(Goomba g) {
		super(g);
		this.lastSpecialAttack = g.lastSpecialAttack;
		this.lastSuperSpecialAttack = g.lastSuperSpecialAttack;
	}
	
	@Override
	public GameCharacter clone() {
		return new Goomba(this);
	}
	
	@Override
	public void onCollision(GameCharacter g) {
		Stack<CharacterState> stack = this.getStateStack();
		if(stack.peek()==CharacterState.ATTACKING) {
			g.reduceHP((int)(this.getPower()),this);
			stack.pop();
		} else if (stack.peek()==CharacterState.SPECIAL_ATTACKING) {
			g.reduceHP((int)(this.getPower()*1.5),this);
			stack.pop();
		} else if (stack.peek()==CharacterState.S_SPECIAL_ATTACKING) {
			g.reduceHP((int)(this.getPower()*2.0),this);
		}
	}
	
	@Override
	public void specialAttack() {
		if(System.currentTimeMillis()-lastSpecialAttack>this.specialAttackDelay && !(this.getStateStack().peek()==CharacterState.ATTACKING || this.getStateStack().peek()==CharacterState.S_SPECIAL_ATTACKING)) {
			lastSpecialAttack = System.currentTimeMillis();
		} else {
			return;
		}
		double oldMaxSpeed = this.getMaxSpeed();
		if(this.getDirection()==EnumDirection.RIGHT) {
			this.setMaxSpeed(30);
			this.setMotionX(30);
		} else if (this.getDirection()==EnumDirection.LEFT) {
			this.setMaxSpeed(30);
			this.setMotionX(-30);
		}
		this.getStateStack().push(CharacterState.SPECIAL_ATTACKING);
		new Thread() {
			public void run() {
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if(Goomba.this.getStateStack().peek()==CharacterState.SPECIAL_ATTACKING) {
					Goomba.this.getStateStack().pop();
				}
				Goomba.this.setMaxSpeed(oldMaxSpeed);
			}
		}.start();
	}

	@SuppressWarnings("static-access")
	@Override
	public void superSpecialAttack() {
		if(System.currentTimeMillis()-lastSuperSpecialAttack>this.superSpecialAttackDelay && this.getStateStack().peek()!=CharacterState.ATTACKING && this.getStateStack().peek()!=CharacterState.SPECIAL_ATTACKING) {
			lastSuperSpecialAttack = System.currentTimeMillis();
		} else {
			return;
		}
		audioPlayer.getSoundEffect().POWERUP.play();
		double oldWidth = this.getWidth();
		double oldHeight = this.getHeight();
		double oldMaxSpeed = this.getMaxSpeed();
		this.setWidth(oldWidth*2.0);
		this.setHeight(oldHeight*2.0);
		this.setMaxSpeed(oldMaxSpeed*2.0);
		this.getStateStack().push(CharacterState.S_SPECIAL_ATTACKING);
		this.setGravityAffected(false);
		
		new Thread() {
			public void run() {
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if(Goomba.this.getStateStack().peek()==CharacterState.S_SPECIAL_ATTACKING) {
					Goomba.this.getStateStack().pop();
				}
				Goomba.this.setGravityAffected(true);
				Goomba.this.setHeight(oldHeight);
				Goomba.this.setWidth(oldWidth);
				Goomba.this.setMaxSpeed(oldMaxSpeed);
			}
		}.start();
	}
	
}
