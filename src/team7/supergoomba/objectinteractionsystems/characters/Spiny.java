package team7.supergoomba.objectinteractionsystems.characters;

import java.io.File;
import java.util.Stack;

import team7.supergoomba.AudioPlayer;
import team7.supergoomba.ObjectInteractionSystem;
import team7.supergoomba.objectinteractionsystems.CharacterState;
import team7.supergoomba.objectinteractionsystems.EnumDirection;
import team7.supergoomba.objectinteractionsystems.GameCharacter;
import team7.supergoomba.objectinteractionsystems.InGameObject;
import team7.supergoomba.objectinteractionsystems.KeyControlEnum;

public class Spiny extends GameCharacter {
	private AudioPlayer audioPlayer;
	
	public Spiny(double x, double y, KeyControlEnum controls, ObjectInteractionSystem oiSystem, boolean isBot) {
		super(x, y, 75, 75, oiSystem, "Spiny", 100, 4, 1, new File("./resources/sprites/characters/spiny"), controls, isBot, 3000, 20000);
		audioPlayer = new AudioPlayer();
	}

	@Deprecated
	public Spiny(Spiny s) {
		super(s);
	}
	
	@Override
	public GameCharacter clone() {
		return new Spiny(this);
	}
	
	@Override
	public void reduceHP(int amount, InGameObject o) {
		if(this.getStateStack().peek()==CharacterState.ATTACKING && o!=null && o instanceof GameCharacter) {
			((GameCharacter)o).reduceHP(amount*2, null);
		} else {
			super.reduceHP(amount, o);
		}
	}
	
	@Override
	public void onCollision(GameCharacter g) {
		Stack<CharacterState> stack = this.getStateStack();
		if(stack.peek()==CharacterState.ATTACKING) {
			g.reduceHP(this.getPower(), this);
			stack.pop();
		} else if (stack.peek()==CharacterState.SPECIAL_ATTACKING) {
			g.reduceHP(this.getPower()*2, this);
		} else if (stack.peek()==CharacterState.S_SPECIAL_ATTACKING) {
			g.reduceHP(this.getPower()*3, this);
			stack.pop();
		}
	}
	
	@SuppressWarnings("static-access")
	@Override
	public void attack() {
		if(this.getOnGround() && System.currentTimeMillis()-this.getLastAttack()>this.attackDelay && !(this.getStateStack().peek()==CharacterState.SPECIAL_ATTACKING || this.getStateStack().peek()==CharacterState.S_SPECIAL_ATTACKING)) {
			this.setLastAttack(System.currentTimeMillis());
		} else {
			return;
		}
		audioPlayer.getSoundEffect().ATTACK.play();
		this.getStateStack().push(CharacterState.ATTACKING);
		double oldMaxSpeed = this.getMaxSpeed();
		this.setMaxSpeed(0.0);
		this.setOnGround(false);
		new Thread() {
			public void run() {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if(Spiny.this.getStateStack().peek()==CharacterState.ATTACKING) {
					Spiny.this.getStateStack().pop();
				}
				Spiny.this.setMaxSpeed(oldMaxSpeed);
				Spiny.this.setOnGround(true);
			}
		}.start();
	}
	
	@SuppressWarnings("static-access")
	@Override
	public void specialAttack() {
		if(System.currentTimeMillis()-this.lastSpecialAttack>this.specialAttackDelay && !(this.getStateStack().peek()==CharacterState.ATTACKING || this.getStateStack().peek()==CharacterState.S_SPECIAL_ATTACKING)) {
			this.lastSpecialAttack = System.currentTimeMillis();
		} else {
			return;
		}
		audioPlayer.getSoundEffect().SHELL.play();
		this.getStateStack().push(CharacterState.SPECIAL_ATTACKING);
		double oldMaxSpeed = this.getMaxSpeed();
		this.setMaxSpeed(this.getMaxSpeed()*12);
		if(this.getDirection()==EnumDirection.RIGHT) {
			this.setMotionX(this.getMaxSpeed());
		} else {
			this.setMotionX(-this.getMaxSpeed());
		}
		new Thread() {
			public void run() {
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if(Spiny.this.getStateStack().peek()==CharacterState.SPECIAL_ATTACKING) {
					Spiny.this.getStateStack().pop();
				}
				Spiny.this.setMaxSpeed(oldMaxSpeed);
			}
		}.start();
	}

	@Override
	public void superSpecialAttack() {
		if(System.currentTimeMillis()-this.lastSuperSpecialAttack>this.superSpecialAttackDelay && !(this.getStateStack().peek()==CharacterState.ATTACKING || this.getStateStack().peek()==CharacterState.SPECIAL_ATTACKING)) {
			this.lastSuperSpecialAttack = System.currentTimeMillis();
		} else {
			return;
		}
		this.getStateStack().push(CharacterState.S_SPECIAL_ATTACKING);
		double oldMaxSpeed = this.getMaxSpeed();
		this.setMaxSpeed(this.getMaxSpeed()*18);
		if(this.getDirection()==EnumDirection.RIGHT) {
			this.setMotionX(this.getMaxSpeed());
		} else {
			this.setMotionX(-this.getMaxSpeed());
		}
		new Thread() {
			public void run() {
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if(Spiny.this.getStateStack().peek()==CharacterState.S_SPECIAL_ATTACKING) {
					Spiny.this.getStateStack().pop();
				}
				Spiny.this.setMaxSpeed(oldMaxSpeed);
			}
		}.start();
		
	}

}
