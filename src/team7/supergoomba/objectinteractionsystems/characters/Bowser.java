package team7.supergoomba.objectinteractionsystems.characters;

import java.io.File;
import java.util.Stack;

import team7.supergoomba.AudioPlayer;
import team7.supergoomba.ObjectInteractionSystem;
import team7.supergoomba.objectinteractionsystems.CharacterState;
import team7.supergoomba.objectinteractionsystems.EnumDirection;
import team7.supergoomba.objectinteractionsystems.GameCharacter;
import team7.supergoomba.objectinteractionsystems.KeyControlEnum;
import team7.supergoomba.objectinteractionsystems.otherobjects.Fireball;

public class Bowser extends GameCharacter {
	private AudioPlayer audioPlayer;
	
	public Bowser(double x, double y, KeyControlEnum controls, ObjectInteractionSystem oiSystem, boolean isBot) {
		super(x, y, 75, 75, oiSystem, "Bowser", 125, 5, 2, new File("./resources/sprites/characters/bowser"), controls, isBot, 1500, 20000);
		audioPlayer = new AudioPlayer();
	}

	@Deprecated
	public Bowser(Bowser b) {
		super(b);
	}
	
	@Override
	public GameCharacter clone() {
		return new Bowser(this);
	}
	
	@Override
	public void onCollision(GameCharacter g) {
		Stack<CharacterState> stack = this.getStateStack();
		if(stack.peek()==CharacterState.ATTACKING) {
			g.reduceHP(this.getPower(), this);
			stack.pop();
		} else if (stack.peek()==CharacterState.SPECIAL_ATTACKING) {
			g.reduceHP(this.getPower()*2, this);
			stack.pop();
		}
		//shouldn't do anything if super special attacking
	}
	
	@Override
	public void specialAttack() {
		if(System.currentTimeMillis()-this.lastSpecialAttack<=this.specialAttackDelay || this.getStateStack().peek()==CharacterState.ATTACKING || this.getStateStack().peek()==CharacterState.S_SPECIAL_ATTACKING) {
			return;
		}
		this.lastSpecialAttack = System.currentTimeMillis();
		this.getStateStack().push(CharacterState.SPECIAL_ATTACKING);
		this.setMotionY(50);
		this.setY(this.getY() - 10);
		this.setOnGround(false);
		new Thread() {
			public void run() {
				try {
					Thread.sleep(3000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if(Bowser.this.getStateStack().peek()==CharacterState.SPECIAL_ATTACKING) {
					Bowser.this.getStateStack().pop();
				}
			}
		}.start();	
	}

	@Override
	public void superSpecialAttack() {
		if(System.currentTimeMillis()-this.lastSuperSpecialAttack<=this.superSpecialAttackDelay || this.getStateStack().peek()==CharacterState.ATTACKING || this.getStateStack().peek()==CharacterState.SPECIAL_ATTACKING) {
			return;
		}
		this.lastSuperSpecialAttack = System.currentTimeMillis();
		this.getStateStack().push(CharacterState.S_SPECIAL_ATTACKING);
		new Thread() {
			@SuppressWarnings("static-access")
			public void run() {
				while(System.currentTimeMillis()-Bowser.this.getLastSuperSpecialAttack()<2500) {
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					double motionX = (Bowser.this.getDirection()==EnumDirection.RIGHT?1.0:-1.0)*(Math.random()*10.0 + 20.0);
					double motionY = Math.random()*40.0 - 20.0;
					audioPlayer.getSoundEffect().FIREBALL.play();
					Fireball f = new Fireball(Bowser.this, Bowser.this.getObjectInteractionSystem(), motionX, motionY, 300);
					Bowser.this.getObjectInteractionSystem().add(f);
				}
				if(Bowser.this.getStateStack().peek()==CharacterState.S_SPECIAL_ATTACKING) {
					Bowser.this.getStateStack().pop();
				}
			}
		}.start();	
	}

}
