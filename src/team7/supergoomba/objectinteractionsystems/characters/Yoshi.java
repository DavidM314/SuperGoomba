package team7.supergoomba.objectinteractionsystems.characters;

import java.io.File;
import java.util.Stack;

import team7.supergoomba.ObjectInteractionSystem;
import team7.supergoomba.objectinteractionsystems.CharacterState;
import team7.supergoomba.objectinteractionsystems.EnumDirection;
import team7.supergoomba.objectinteractionsystems.GameCharacter;
import team7.supergoomba.objectinteractionsystems.InGameObject;
import team7.supergoomba.objectinteractionsystems.KeyControlEnum;
import team7.supergoomba.objectinteractionsystems.otherobjects.YoshiEgg;

public class Yoshi extends GameCharacter {

	public Yoshi(double x, double y, KeyControlEnum controls, ObjectInteractionSystem oiSystem, boolean isBot) {
		super(x, y, 75, 75, oiSystem, "Yoshi", 75, 1, 4, new File("./resources/sprites/characters/yoshi"), controls, isBot, 1500, 20000);
	}

	@Deprecated
	public Yoshi(Yoshi y) {
		super(y);
	}
	
	@Override
	public GameCharacter clone() {
		return new Yoshi(this);
	}
	
	@Override
	public void reduceHP(int amount, InGameObject src) {
		if(this.getStateStack().peek()==CharacterState.S_SPECIAL_ATTACKING) {
			return;
		}
		super.reduceHP(amount, src);
	}
	
	@Override
	public void onCollision(GameCharacter g) {
		Stack<CharacterState> stack = this.getStateStack();
		if(stack.peek()==CharacterState.ATTACKING) {
			g.reduceHP(this.getPower(), this);
		} else if (stack.peek()==CharacterState.SPECIAL_ATTACKING) {
			g.reduceHP(this.getPower()*2, this);
		}
	}
	
	@Override
	public void specialAttack() {
		if(System.currentTimeMillis()-this.lastSpecialAttack<=this.specialAttackDelay || this.getStateStack().peek()==CharacterState.ATTACKING || this.getStateStack().peek()==CharacterState.S_SPECIAL_ATTACKING) {
			return;
		}
		this.lastSpecialAttack = System.currentTimeMillis();
		this.getStateStack().push(CharacterState.SPECIAL_ATTACKING);
		if(this.getDirection()==EnumDirection.LEFT) {
			this.setX(this.getX() - 50);
		} else {
			this.setX(this.getX() + 50);
		}
		new Thread() {
			public void run() {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if(Yoshi.this.getStateStack().peek()==CharacterState.SPECIAL_ATTACKING) {
					Yoshi.this.getStateStack().pop();
				}
			}
		}.start();
	}

	@Override
	public void superSpecialAttack() {
		if(System.currentTimeMillis()-this.lastSuperSpecialAttack<=this.superSpecialAttackDelay || this.getStateStack().peek()==CharacterState.ATTACKING || this.getStateStack().peek()==CharacterState.SPECIAL_ATTACKING) {
			return;
		}
		this.lastSuperSpecialAttack = System.currentTimeMillis();
		this.getStateStack().push(CharacterState.S_SPECIAL_ATTACKING);
		
		new Thread() {
			public void run() {
				while(System.currentTimeMillis()-Yoshi.this.lastSuperSpecialAttack<2500) {
					Yoshi.this.setMotionX(0);
					Yoshi.this.setMotionY(0);
				}
				if(Yoshi.this.getStateStack().peek()==CharacterState.S_SPECIAL_ATTACKING) {
					Yoshi.this.getStateStack().pop();
					double x = Yoshi.this.getDirection()==EnumDirection.RIGHT?(Yoshi.this.getX() - 100):(Yoshi.this.getX() + Yoshi.this.getWidth() + 100);
					YoshiEgg egg = new YoshiEgg(x, Yoshi.this.getY() + Yoshi.this.getHeight() - 100, Yoshi.this.getObjectInteractionSystem(), Yoshi.this);
					Yoshi.this.getObjectInteractionSystem().add(egg);
				}
			}
		}.start();	
	}

}
