package team7.supergoomba.objectinteractionsystems;

public enum CharacterState {
	STILL,
	JUMPING,
	MOVING,
	ATTACKING,
	SPECIAL_ATTACKING,
	S_SPECIAL_ATTACKING
}
