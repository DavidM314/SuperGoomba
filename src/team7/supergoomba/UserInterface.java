package team7.supergoomba;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Image;
import java.io.File;

import javax.imageio.ImageIO;

import team7.supergoomba.objectinteractionsystems.GameCharacter;
import team7.supergoomba.renderer2d.PaintableObject;

public class UserInterface implements PaintableObject {

	private final File cloud = new File("./resources/user interfaces/cloud.png");
	private final File gradient = new File("./resources/user interfaces/gradient.png");
	private Image cloudImg;
	private Image gradientImg;
	private Game game;
	private Renderer2D renderer;
	
	public UserInterface(Game game, Renderer2D renderer) {
		this.game = game;
		this.renderer = renderer;
		try {
			this.cloudImg = ImageIO.read(cloud);
			this.gradientImg = ImageIO.read(gradient);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void drawChargeBar(Graphics2D g, int x, int y, int rectWidth, int rectHeight, GameCharacter player) {
		g.drawImage(gradientImg, x, y, (int)(rectWidth*Math.min((double)(System.currentTimeMillis()-player.getLastSuperSpecialAttack())/(double)player.getSuperSpecialAttackDelay(),1.0)), rectHeight, null);
		g.drawRect(x, y, rectWidth, rectHeight);
	}
	
	private void drawHealthBar(Graphics2D g, int x, int y, int rectWidth, int rectHeight, int arcWidth, int arcHeight, GameCharacter player, double height, double width, int playerNo) {
		g.setColor(Color.RED);
		g.fillRoundRect(x, y, rectWidth, rectHeight, arcWidth, arcHeight);
		g.setColor(Color.GREEN);
		if(x + (int)(((double)(rectWidth))*player.getPercentHealth()) - arcWidth>=x + arcWidth) {
			g.fillRoundRect(x, y, (int)(((double)(rectWidth))*player.getPercentHealth()), rectHeight, arcWidth, arcHeight);
			g.fillRect(x + (int)(((double)(rectWidth))*player.getPercentHealth()) - arcWidth, y, arcWidth, rectHeight);
		} else {
			g.fillRoundRect(x, y, (int)(((double)(rectWidth))*player.getPercentHealth()), rectHeight, arcWidth, arcHeight);
		}
		g.setColor(Color.BLACK);
		g.setFont(new Font("Comic Sans MS", Font.PLAIN, (int)(height/32.03333333333333)));
		g.drawImage(cloudImg,x,y-40,(int)(rectWidth/1.6),(int)(rectHeight*1.3),null);
		String name = (player.getBot()?"CPU: ":"P" + playerNo + ": ") + player.getName();
		g.drawString(name, x + (int)(rectWidth/1.6)/2 - g.getFontMetrics().stringWidth(name)/2, (y-40)+((int)(rectHeight*1.3))/2 + g.getFontMetrics().getHeight()/4);
		g.drawString(Integer.toString(player.getHealth()), x + rectWidth - 20 - g.getFontMetrics().stringWidth(Integer.toString(player.getHealth())), y + (int)(height/13.728571428571428) - 30);
	}
	
	@Override
	public void paint(Graphics2D g) {
		g.setColor(Color.RED);
		double width = renderer.getWidth();
		double height = renderer.getHeight();
		int rectWidth = (int) (width/2.56);
		int rectHeight = (int) (height/16.016666666666666);
		drawHealthBar(g,(int)(width) - rectWidth - 30, (int)(height) - rectHeight - 20, rectWidth, rectHeight, (int)(width/18.285714285714285), (int)(height/13.728571428571428), game.getPlayer(2), height, width, 2);
		drawHealthBar(g,30, (int)(height)-rectHeight-20, rectWidth, rectHeight, (int)(width/18.285714285714285), (int)(height/13.728571428571428),game.getPlayer(1), height, width, 1);
		drawChargeBar(g,(int)(width) - rectWidth - 30 + rectWidth*15/24, (int)(height) - rectHeight - 20 - rectHeight/6, rectWidth/3, rectHeight/3, game.getPlayer(2));
		drawChargeBar(g,30 + rectWidth*15/24 , (int)(height)-rectHeight-20 - rectHeight/6, rectWidth/3, rectHeight/3, game.getPlayer(1));
		drawTime(g,width,height);
	}
	
	private void drawTime(Graphics2D g, double width, double height) {
		g.setFont(new Font("Comic Sans MS", Font.PLAIN, (int)(height/32.03333333333333)));
		String time = game.getTimeStr();
		g.drawString(time, (int) (width-g.getFontMetrics().stringWidth(time)), g.getFontMetrics().getHeight() - 10);
	}

}
